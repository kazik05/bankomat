<header class="header header-transparent" id="header-main">
    <!-- Main navbar -->
    <nav class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light bg-white" id="navbar-main">
        <div class="container px-lg-0">
            <!-- Logo -->
            <a class="navbar-brand mr-lg-5" href="{{ url('/') }}">
                <img alt="Image placeholder" src="{{ asset('assets/images/brand/favicon.png') }}" id="navbar-logo" style="height: 50px;">
            </a>
            <!-- Navbar collapse trigger -->
            <button class="navbar-toggler pr-0"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbar-main-collapse"
                    aria-controls="navbar-main-collapse"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <!-- Navbar nav -->
            <div class="collapse navbar-collapse" id="navbar-main-collapse">
                <ul class="navbar-nav align-items-lg-center">
                    <!-- Home - Overview  -->
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/') }}">
                            {{ \App\Message::get('navbar:home') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ request()->is('loan') ? ' active' : '' }}" href="{{ url('loan') }}" data-scroll-to="">
                            {{ \App\Message::get('navbar:loan') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ request()->is('cash') ? ' active' : '' }}" href="{{ url('cash') }}" data-scroll-to="">
                            {{ \App\Message::get('navbar:cash') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ request()->is('credit-card') ? ' active' : '' }}" href="{{ url('credit-card') }}" data-scroll-to="">
                            {{ \App\Message::get('navbar:credit-card') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ request()->is('reviews') ? ' active' : '' }}" href="{{ url('reviews') }}" data-scroll-to="">
                            {{ \App\Message::get('navbar:reviews') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ request()->is('faq') ? ' active' : '' }}" href="{{ url('faq') }}" data-scroll-to="">
                            {{ \App\Message::get('navbar:faq') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ request()->is('blog') ? ' active' : '' }}" href="{{ url('blog') }}">
                            {{ \App\Message::get('navbar:blog') }}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
