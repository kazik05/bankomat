<footer id="footer-main" class="footer position-relative pt-9 footer-dark bg-gradient-primary">
    <div class="shape-container" data-shape-position="top" data-shape-orientation="inverse">
        <img alt="Image placeholder" src="{{ asset('assets/images/svg/shapes/curve-1.svg') }}" class="svg-inject">
    </div>
    <div class="container pt-5">
        <div class="row pt-md">
            <div class="col-lg-4 mb-3 text-center text-sm-left mb-lg-0">
                <p>{!! App\Message::get('footer:description') !!}</p>
            </div>
            <div class="col-lg-2 d-none d-lg-block"></div>
            <div class="col-lg-2 col-sm-6 text-center text-sm-left col-12 mb-3 mb-lg-0">
                <h6 class="heading mb-3">{{ App\Message::get('footer:about-title') }}</h6>
                <ul class="list-unstyled text-small">
                    <li><a href="{{ url('/blog') }}">{{ App\Message::get('footer:blog') }}</a></li>
                    <li><a href="{{ url('/about') }}">{{ App\Message::get('footer:about') }}</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-sm-6 text-center text-sm-left col-12 mb-3 mb-lg-0">
                <h6 class="heading mb-3">{{ App\Message::get('footer:info-title') }}</h6>
                <ul class="list-unstyled">
                    <li><a href="{{ url('/loan') }}">{{ App\Message::get('footer:loan') }}</a></li>
                    <li><a href="{{ url('/online') }}">{{ App\Message::get('footer:online') }}</a></li>
                    <li><a href="{{ url('/card') }}">{{ App\Message::get('footer:card') }}</a></li>
                    <li><a href="{{ url('/cash') }}">{{ App\Message::get('footer:cash') }}</a></li>
                    <li><a href="{{ url('/credit-card') }}">{{ App\Message::get('footer:credit-card') }}</a></li>
                    <li><a href="{{ url('/faq') }}">{{ App\Message::get('footer:faq') }}</a></li>
                    <li><a href="{{ url('/courses') }}">{{ App\Message::get('footer:courses') }}</a></li>
                </ul>
            </div>
        </div>
        <div class="row align-items-center justify-content-md-between py-4 mt-4 delimiter-top">
            <div class="col-md-6">
                <div class="copyright text-sm font-weight-bold text-center text-md-left">
                    &copy; {{ date('Y') }} <a href="{{ url('/') }}" class="font-weight-bold" target="_blank">{{ App\Message::get('footer:brand') }}</a>.
                    {{ App\Message::get('footer:copyrights') }}
                </div>
            </div>
            <div class="col-md-6">
                <ul class="nav justify-content-center justify-content-md-end mt-3 mt-md-0">
                    @if(App\Setting::get('social_telegram'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ App\Setting::get('social_telegram') }}" target="_blank">
                                <i class="fab fa-telegram"></i>
                            </a>
                        </li>
                    @endif
                    @if(App\Setting::get('social_vk'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ App\Setting::get('social_vk') }}" target="_blank">
                                <i class="fab fa-vk"></i>
                            </a>
                        </li>
                    @endif
                    @if(App\Setting::get('social_facebook'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ App\Setting::get('social_facebook') }}" target="_blank">
                                <i class="fab fa-facebook"></i>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</footer>
