<div class="mb-5 mb-lg-0" data-animate-hover="1">
    <div class="animate-this">
        <a href="{{ $post->url }}">
            <img alt="Image placeholder" class="img-fluid rounded shadow" src="{{ $post->image }}">
        </a>
    </div>
    <div class="pt-4">
        <small class="text-uppercase">
            <span class="fas fa-clock mr-2"></span>
            {{ \Jenssegers\Date\Date::parse($post->created_at)->format('d M, Y') }}
        </small>
        <small class="text-uppercase ml-2">
            <span class="fas fa-comment mr-2"></span>
            <span class="disqus-comment-count" data-disqus-identifier="{{ $post->id }}">0 комментариев</span>
        </small>
        <h5><a href="{{ $post->url }}">{{ $post->title }}</a></h5>
        <p class="mt-3">{{ $post->description }}</p>
    </div>
</div>
