@extends('front.layouts.main')

@section('meta')
    <title>{{ $post->seo_title }} - {{ config('app.name') }}</title>
    <meta name="keywords" content="{{ $post->seo_keywords }}">
    <meta name="description" content="{{ $post->seo_description }}">
    <meta name="robots" content="{{ $post->robots }}">
@endsection

@section('main')
    @include('front.partials.navbar-light')

    <div class="main-content">
        <section class="slice slice-sm" data-offset-top="#header-main">
            <div class="container pt-6">
                <div class="row justify-content-center">
                    <div class="col-md-9">
                        <h1 class="lh-150 mb-3"><a href="{{ url('/blog') }}">{{ App\Message::get('blog:title') }}</a> → {{ $post->title }}</h1>
                    </div>
                </div>
            </div>
        </section>

        <section class="slice">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-9">
                        <!-- Article body -->
                        <article>
                            <figure class="figure">
                                <img alt="Image placeholder" src="{{ $post->image }}" class="img-fluid rounded">
                            </figure>

                            {!! $post->content !!}
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <section class="slice">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-9">
                        <div id="disqus_thread"></div>
                        <script>
                            var disqus_config = function () {
                                this.page.url = '{{ $post->url }}';
                                this.page.identifier = '{{ $post->id }}';
                            };
                            (function() { // DON'T EDIT BELOW THIS LINE
                                var d = document, s = d.createElement('script');
                                s.src = 'https://bankomat-su.disqus.com/embed.js';
                                s.setAttribute('data-timestamp', +new Date());
                                (d.head || d.body).appendChild(s);
                            })();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                    </div>
                </div>
            </div>
        </section>

        @if(count($similar))
            <section class="slice-sm">
                <div class="container">
                    <div class="mb-5 text-center">
                        <h3 class=" mt-4">{{ App\Message::get('blog:similar-title') }}</h3>
                    </div>
                    <div class="row">
                        @foreach($similar as $similarPost)
                            <div class="col-lg-4">
                                @include('front.partials.post-preview', ['post' => $similarPost])
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="mt-4 text-center">
                    <a href="{{ url('/blog') }}" class="btn btn-primary rounded-pill btn-icon mt-5">
                        <span class="btn-inner--icon"><i class="fas fa-angle-right"></i></span>
                        <span class="btn-inner--text">{{ App\Message::get('blog:view-all') }}</span>
                    </a>
                </div>
            </section>
        @endif
    </div>
@endsection

@push('scripts')
    <script id="dsq-count-scr" src="//bankomat-su.disqus.com/count.js" async></script>
@endpush
