<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @yield('meta')

        <link rel="icon" href="{{ asset('assets/images/brand/favicon.png') }}" type="image/png">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Shadows+Into+Light">
        <link rel="stylesheet" href="{{ asset('assets/css/front.css') }}">
        @stack('styles')

        @if(config('app.env') !== 'local')
            {!! \App\Setting::get('head_append') !!}
        @endif
    </head>
    <body class="bg-dark bg-noise">
        @yield('main')

        @include('front.partials.footer')
        <script src="{{ asset('assets/js/front.js') }}"></script>
        @stack('scripts')

        @if(config('app.env') !== 'local')
            {!! \App\Setting::get('body_append') !!}
        @endif
    </body>
</html>
