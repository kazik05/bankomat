@extends('front.layouts.main')

@section('meta')
    <title>{{ $page->seo_title }} - {{ config('app.name') }}</title>
    <meta name="keywords" content="{{ $page->seo_keywords }}">
    <meta name="description" content="{{ $page->seo_description }}">
    <meta name="robots" content="{{ $page->robots }}">
@endsection

@section('main')
    @include('front.partials.navbar-light')

    <div class="main-content">
        <section class="slice slice-sm" data-offset-top="#header-main">
            <div class="container pt-6">
                <div class="row justify-content-center">
                    <div class="col-md-9">
                        <h1 class="lh-150 mb-3 text-center">{{ $page->title }}</h1>
                    </div>
                </div>
            </div>
        </section>

        @if($page->content)
            <section class="slice">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-9">
                            <article>
                                {!! $page->content !!}
                            </article>
                        </div>
                    </div>
                </div>
            </section>
        @endif

        <section class="slice">
            <div class="container">
                <div id="script-here"></div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset($page->details['script']) }}"></script>
@endpush
