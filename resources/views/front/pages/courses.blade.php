@extends('front.layouts.main')

@section('meta')
    <title>{{ $page->seo_title }} - {{ config('app.name') }}</title>
    <meta name="keywords" content="{{ $page->seo_keywords }}">
    <meta name="description" content="{{ $page->seo_description }}">
    <meta name="robots" content="{{ $page->robots }}">
@endsection

@section('main')
    @include('front.partials.navbar-light')

    <div class="main-content">
        <section class="slice slice-sm" data-offset-top="#header-main">
            <div class="container pt-6">
                <div class="row justify-content-center">
                    <div class="col-md-9">
                        <h1 class="lh-150 mb-3 text-center">{{ $page->title }}</h1>
                    </div>
                </div>
            </div>
        </section>

        @if($page->content)
            <section class="slice">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-9">
                            <article>
                                {!! $page->content !!}
                            </article>
                        </div>
                    </div>
                </div>
            </section>
        @endif

        <section class="slice">
            <div class="container">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="d-none d-md-table-cell">Название</th>
                            <th class="text-right">Курс</th>
                            <th class="text-right d-none d-sm-table-cell">Дата обновлния</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($courses as $course)
                            <tr>
                                <td class="d-none d-md-table-cell">{{ $course->name }}</td>
                                <td class="text-right">
                                    <span class="d-inline-block" style="width:100px">
                                        {{ number_format($course->nominal, 2) }}
                                        {{ $course->char_code }}
                                    </span>
                                    =
                                    <span>{{ number_format($course->value, 2) }} RUB</span>
                                </td>
                                <td class="text-right d-none d-sm-table-cell">{{ $course->updated_at->format('d.m.Y') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>
@endsection
