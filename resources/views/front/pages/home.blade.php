@extends('front.layouts.main')

@section('meta')
    <title>{{ $page->seo_title }} - {{ config('app.name') }}</title>
    <meta name="keywords" content="{{ $page->seo_keywords }}">
    <meta name="description" content="{{ $page->seo_description }}">
    <meta name="robots" content="{{ $page->robots }}">
@endsection

@section('main')
    <header class="header header-transparent" id="header-main">
        <!-- Main navbar -->
        <nav class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-dark bg-dark" id="navbar-main">
            <div class="container px-lg-0">
                <!-- Logo -->
                <a class="navbar-brand mr-lg-5" href="{{ url('/') }}">
                    <img alt="Image placeholder" src="{{ asset('assets/images/brand/favicon.png') }}" id="navbar-logo" style="height: 50px;">
                </a>
                <!-- Navbar collapse trigger -->
                <button class="navbar-toggler pr-0"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbar-main-collapse"
                        aria-controls="navbar-main-collapse"
                        aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <!-- Navbar nav -->
                <div class="collapse navbar-collapse" id="navbar-main-collapse">
                    <ul class="navbar-nav align-items-lg-center">
                        <!-- Home - Overview  -->
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ url('/') }}">
                                {{ \App\Message::get('navbar:home') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('loan') }}" data-scroll-to="">
                                {{ \App\Message::get('navbar:loan') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('cash') }}" data-scroll-to="">
                                {{ \App\Message::get('navbar:cash') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('credit-card') }}" data-scroll-to="">
                                {{ \App\Message::get('navbar:credit-card') }}
                            </a>
                        </li>
                        @if(count($reviews) > 0)
                            <li class="nav-item">
                                <a class="nav-link" href="#sct-reviews" data-scroll-to="">
                                    {{ \App\Message::get('navbar:reviews') }}
                                </a>
                            </li>
                        @endif
                        @if(count($faq) > 0)
                            <li class="nav-item">
                                <a class="nav-link" href="#sct-faq" data-scroll-to="">
                                    {{ \App\Message::get('navbar:faq') }}
                                </a>
                            </li>
                        @endif
                        @if(count($posts) > 0)
                            <li class="nav-item">
                                <a class="nav-link" href="#sct-blog">
                                    {{ \App\Message::get('navbar:blog') }}
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <div class="main-content">
        <section class="slice slice-xl bg-cover bg-size--cover"
                 data-offset-top="#header-main"
                 style="background-image: url(/assets/images/backgrounds/img-16.jpg); background-position: center center; padding-top: 89px;">
            <span class="mask bg-dark opacity-3"></span>
            <div class="container py-5 pt-lg-7 position-relative zindex-100">
                <div class="row">
                    <div class="col-lg-6 text-center text-lg-left">
                        <div class="">
                            <h2 class="text-white my-4">
                                <span class="display-4 font-weight-light">{{ App\Message::get('call-to-action:title') }}</span>
                            </h2>
                            <p class="lead text-white">{{ App\Message::get('call-to-action:text') }}</p>
                        </div>
                    </div>
                </div>
                <div class="row mt-6 d-flex">
                    <div class="col-lg-4">
                        <a href="{{ url('loan') }}" class="card bg-dark border-0 hover-shadow-lg hover-translate-y-n10">
                            <div class="card-body pb-2">
                                <div class="d-flex align-items-start">
                                    <div class="icon bg-gradient-primary text-white rounded-circle icon-shape">
                                        <i class="fas fa-money-bill-wave-alt"></i>
                                    </div>
                                    <div class="icon-text pl-4">
                                        <h5 class="ma-0  text-white">{{ App\Message::get('loan:title') }}</h5>
                                        <p class="mb-0 text-muted">{{ App\Message::get('loan:description') }}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <a href="{{ url('online') }}" class="card bg-dark border-0 hover-shadow-lg hover-translate-y-n10">
                            <div class="card-body pb-2">
                                <div class="d-flex align-items-start">
                                    <div class="icon bg-gradient-primary text-white rounded-circle icon-shape">
                                        <i class="fas fa-wifi"></i>
                                    </div>
                                    <div class="icon-text pl-4">
                                        <h5 class="ma-0  text-white">{{ App\Message::get('online:title') }}</h5>
                                        <p class="mb-0 text-muted">{{ App\Message::get('online:description') }}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <a href="{{ url('card') }}" class="card bg-dark border-0 hover-shadow-lg hover-translate-y-n10">
                            <div class="card-body pb-2">
                                <div class="d-flex align-items-start">
                                    <div class="icon bg-gradient-primary text-white rounded-circle icon-shape">
                                        <i class="fas fa-credit-card"></i>
                                    </div>
                                    <div class="icon-text pl-4">
                                        <h5 class="ma-0  text-white">{{ App\Message::get('card:title') }}</h5>
                                        <p class="mb-0 text-muted">{{ App\Message::get('card:description') }}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <a href="{{ url('cash') }}" class="card bg-dark border-0 hover-shadow-lg hover-translate-y-n10">
                            <div class="card-body pb-2">
                                <div class="d-flex align-items-start">
                                    <div class="icon bg-gradient-primary text-white rounded-circle icon-shape">
                                        <i class="fas fa-coins"></i>
                                    </div>
                                    <div class="icon-text pl-4">
                                        <h5 class="ma-0  text-white">{{ App\Message::get('cash:title') }}</h5>
                                        <p class="mb-0 text-muted">{{ App\Message::get('cash:description') }}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <a href="{{ url('credit-card') }}" class="card bg-dark border-0 hover-shadow-lg hover-translate-y-n10">
                            <div class="card-body pb-2">
                                <div class="d-flex align-items-start">
                                    <div class="icon bg-gradient-primary text-white rounded-circle icon-shape">
                                        <i class="fab fa-cc-visa"></i>
                                    </div>
                                    <div class="icon-text pl-4">
                                        <h5 class="ma-0  text-white">{{ App\Message::get('credit-card:title') }}</h5>
                                        <p class="mb-0 text-muted">{{ App\Message::get('credit-card:description') }}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="shape-container" data-shape-position="bottom" style="height: 567px;">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" x="0px" y="0px" viewBox="0 0 1000 300" style="enable-background:new 0 0 1000 300;" xml:space="preserve" class="ie-shape-wave-1 fill-section-secondary">
                <path d="M 0 246.131 C 0 246.131 31.631 250.035 47.487 249.429 C 65.149 248.755 82.784 245.945 99.944 241.732 C 184.214 221.045 222.601 171.885 309.221 166.413 C 369.892 162.581 514.918 201.709 573.164 201.709 C 714.375 201.709 772.023 48.574 910.547 21.276 C 939.811 15.509 1000 24.025 1000 24.025 L 1000 300.559 L -0.002 300.559 L 0 246.131 Z"></path>
              </svg>
            </div>
        </section>

        {{--
        <section class="slice">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <blockquote class="blockquote blockquote-card py-5 px-5 rounded-right bg-neutral hover-scale-110">
                            <p class="lead">{{ App\Message::get('quote:text') }}</p>
                            <footer class="blockquote-footer">{{ App\Message::get('quote:author') }}</footer>
                        </blockquote>
                    </div>
                </div>
            </div>
        </section>
        --}}

        @if(count($reviews) > 0)
            <section class="slice slice-lg" id="sct-reviews">
                <div class="container">
                    <div class="mb-5 text-center">
                        <h3>{{ App\Message::get('reviews:title') }}</h3>
                        <div class="fluid-paragraph mt-3">
                            <p class="lead lh-180">{{ App\Message::get('reviews:description') }}</p>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-11">
                            <div class="swiper-js-container overflow-hidden">
                                <div class="swiper-container" data-swiper-items="1" data-swiper-space-between="0" data-swiper-sm-items="2" data-swiper-xl-items="3">
                                    <div class="swiper-wrapper">
                                        @foreach($reviews as $review)
                                            <div class="swiper-slide p-4">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <div>
                                                                <img alt="Image placeholder" src="{{ $review->image }}" class="avatar rounded-circle">
                                                            </div>
                                                            <div class="pl-3">
                                                                <h5 class="h6 mb-0">{{ $review->name }}</h5>
                                                                <small class="d-block text-muted">{{ $review->platform_name }}</small>
                                                            </div>
                                                        </div>
                                                        <p class="mt-4 lh-180">{{ $review->review }}</p>
                                                        <span class="static-rating static-rating-sm">
                                                            @for($i = 0; $i < $review->rating; $i++)
                                                                <i class="star fas fa-star voted"></i>
                                                            @endfor
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <!-- Add Pagination -->
                                <div class="swiper-pagination w-100 mt-4 d-flex align-items-center justify-content-center"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif

        @if(count($posts) > 0)
            <section class="slice-sm" id="sct-blog">
                <div class="container">
                    <div class="mb-5 text-center">
                        <h3 class=" mt-4">{{ App\Message::get('blog:title') }}</h3>
                    </div>
                    <div class="row">
                        @foreach($posts as $post)
                            <div class="col-lg-4">
                                @include('front.partials.post-preview', ['post' => $post])
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="text-center">
                    <a href="{{ url('/blog') }}" class="btn btn-light btn-icon rounded-pill hover-scale-110 mt-5">
                        <span class="btn-inner--text">
                            {{ App\Message::get('blog:view-all') }}
                        </span>
                        <span class="btn-inner--icon"><i class="fas fa-angle-right"></i></span>
                    </a>
                </div>
            </section>
        @endif

        @if(count($faq) > 0)
            <section class="slice slice-sm bg-section-secondary" id="sct-faq">
                <div class="container">
                    <div class="mb-5 text-center">
                        <h3 class=" mt-4">{{ App\Message::get('faq:title') }}</h3>
                        <div class="fluid-paragraph mt-3">
                            <p class="lead lh-180">{{ App\Message::get('faq:description') }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-6">
                            <div id="accordion-1" class="accordion accordion-spaced">
                                @for($i = 0; $i < ceil(count($faq) / 2); $i++)
                                    <div class="card">
                                        <div class="card-header py-4"
                                             id="heading-1-{{ $i }}"
                                             data-toggle="collapse"
                                             role="button"
                                             data-target="#collapse-1-{{ $i }}"
                                             aria-expanded="false"
                                             aria-controls="collapse-1-{{ $i }}">
                                            <h6 class="mb-0"><i class="{{ $faq[$i]->icon }} mr-3"></i>{{ $faq[$i]->question }}</h6>
                                        </div>
                                        <div id="collapse-1-{{ $i }}" class="collapse" aria-labelledby="heading-1-{{ $i }}" data-parent="#accordion-1">
                                            <div class="card-body">
                                                <p>{{ $faq[$i]->answer }}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endfor
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div id="accordion-2" class="accordion accordion-spaced">
                                @for($i = ceil(count($faq) / 2); $i < count($faq); $i++)
                                    <div class="card">
                                        <div class="card-header py-4"
                                             id="heading-1-{{ $i }}"
                                             data-toggle="collapse"
                                             role="button"
                                             data-target="#collapse-1-{{ $i }}"
                                             aria-expanded="false"
                                             aria-controls="collapse-1-{{ $i }}">
                                            <h6 class="mb-0"><i class="{{ $faq[$i]->icon }} mr-3"></i>{{ $faq[$i]->question }}</h6>
                                        </div>
                                        <div id="collapse-1-{{ $i }}" class="collapse" aria-labelledby="heading-1-{{ $i }}" data-parent="#accordion-1">
                                            <div class="card-body">
                                                <p>{{ $faq[$i]->answer }}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endfor
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <a href="{{ url('/faq') }}" class="btn btn-light btn-icon rounded-pill hover-scale-110 mt-5">
                        <span class="btn-inner--text">
                            {{ App\Message::get('faq:view-all') }}
                        </span>
                        <span class="btn-inner--icon"><i class="fas fa-angle-right"></i></span>
                    </a>
                </div>
            </section>
        @endif
    </div>
@endsection

@push('scripts')
    <script id="dsq-count-scr" src="//bankomat-su.disqus.com/count.js" async></script>
@endpush
