@extends('front.layouts.main')

@section('meta')
    <title>{{ $page->seo_title }} - {{ config('app.name') }}</title>
    <meta name="keywords" content="{{ $page->seo_keywords }}">
    <meta name="description" content="{{ $page->seo_description }}">
    <meta name="robots" content="{{ $page->robots }}">
@endsection

@section('main')
    @include('front.partials.navbar-light')

    <div class="main-content">
        <section class="slice slice-sm" data-offset-top="#header-main">
            <div class="container pt-6">
                <div class="row justify-content-center">
                    <div class="col-md-9">
                        <h1 class="lh-150 mb-3 text-center">{{ $page->title }}</h1>
                    </div>
                </div>
            </div>
        </section>

        @if($page->content)
            <section class="slice">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-9">
                            <article>
                                {!! $page->content !!}
                            </article>
                        </div>
                    </div>
                </div>
            </section>
        @endif

        @if(count($posts) == 0)
            <section class="slice slice-sm" data-offset-top="#header-main">
                <div class="container pt-6">
                    <div class="row justify-content-center">
                        <div class="col-md-9">
                            <h1 class="lh-150 mb-3 text-center">{{ App\Message::get('blog:empty') }}</h1>
                        </div>
                    </div>
                </div>
            </section>
        @else
            <section class="slice-sm">
                <div class="container">
                    <div class="row">
                        @foreach($posts as $post)
                            <div class="col-lg-4">
                                @include('front.partials.post-preview', ['post' => $post])
                            </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-12 pt-5 d-flex">
                            <div class="ml-auto mr-auto">
                                {!! $posts->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    </div>
@endsection

@push('scripts')
    <script id="dsq-count-scr" src="//bankomat-su.disqus.com/count.js" async></script>
@endpush
