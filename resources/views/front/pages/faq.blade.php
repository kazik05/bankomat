@extends('front.layouts.main')

@section('meta')
    <title>{{ $page->seo_title }} - {{ config('app.name') }}</title>
    <meta name="keywords" content="{{ $page->seo_keywords }}">
    <meta name="description" content="{{ $page->seo_description }}">
    <meta name="robots" content="{{ $page->robots }}">
@endsection

@section('main')
    @include('front.partials.navbar-light')

    <div class="main-content">
        <section class="slice slice-sm" data-offset-top="#header-main">
            <div class="container pt-6">
                <div class="row justify-content-center">
                    <div class="col-md-9">
                        <h1 class="lh-150 mb-3 text-center">{{ $page->title }}</h1>
                    </div>
                </div>
            </div>
        </section>

        @if($page->content)
            <section class="slice">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-9">
                            <article>
                                {!! $page->content !!}
                            </article>
                        </div>
                    </div>
                </div>
            </section>
        @endif

        <section class="slice slice-sm" id="sct-faq">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6">
                        <div id="accordion-1" class="accordion accordion-spaced">
                            @for($i = 0; $i < ceil(count($faqs) / 2); $i++)
                                <div class="card">
                                    <div class="card-header py-4"
                                         id="heading-1-{{ $i }}"
                                         data-toggle="collapse"
                                         role="button"
                                         data-target="#collapse-1-{{ $i }}"
                                         aria-expanded="false"
                                         aria-controls="collapse-1-{{ $i }}">
                                        <h6 class="mb-0"><i class="{{ $faqs[$i]->icon }} mr-3"></i>{{ $faqs[$i]->question }}</h6>
                                    </div>
                                    <div id="collapse-1-{{ $i }}" class="collapse" aria-labelledby="heading-1-{{ $i }}" data-parent="#accordion-1">
                                        <div class="card-body">
                                            <p>{{ $faqs[$i]->answer }}</p>
                                        </div>
                                    </div>
                                </div>
                            @endfor
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div id="accordion-2" class="accordion accordion-spaced">
                            @for($i = ceil(count($faqs) / 2); $i < count($faqs); $i++)
                                <div class="card">
                                    <div class="card-header py-4"
                                         id="heading-1-{{ $i }}"
                                         data-toggle="collapse"
                                         role="button"
                                         data-target="#collapse-1-{{ $i }}"
                                         aria-expanded="false"
                                         aria-controls="collapse-1-{{ $i }}">
                                        <h6 class="mb-0"><i class="{{ $faqs[$i]->icon }} mr-3"></i>{{ $faqs[$i]->question }}</h6>
                                    </div>
                                    <div id="collapse-1-{{ $i }}" class="collapse" aria-labelledby="heading-1-{{ $i }}" data-parent="#accordion-1">
                                        <div class="card-body">
                                            <p>{{ $faqs[$i]->answer }}</p>
                                        </div>
                                    </div>
                                </div>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
