@extends('front.layouts.main')

@section('meta')
    <title>{{ $page->seo_title }} - {{ config('app.name') }}</title>
    <meta name="keywords" content="{{ $page->seo_keywords }}">
    <meta name="description" content="{{ $page->seo_description }}">
    <meta name="robots" content="{{ $page->robots }}">
@endsection

@section('main')
    @include('front.partials.navbar-light')

    <div class="main-content">
        <section class="slice slice-sm" data-offset-top="#header-main">
            <div class="container pt-6">
                <div class="row justify-content-center">
                    <div class="col-md-9">
                        <h1 class="lh-150 mb-3 text-center">{{ $page->title }}</h1>
                    </div>
                </div>
            </div>
        </section>

        @if($page->content)
            <section class="slice">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-9">
                            <article>
                                {!! $page->content !!}
                            </article>
                        </div>
                    </div>
                </div>
            </section>
        @endif

        <section class="slice slice-lg">
            <div class="container">
                <div class="card-columns">
                    @foreach($reviews as $review)
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <img alt="Image placeholder" src="{{ $review->image }}" class="avatar  rounded-circle">
                                    </div>
                                    <div class="pl-3">
                                        <h5 class="h6 mb-0">{{ $review->name }}</h5>
                                    </div>
                                </div>
                                <p class="mt-4 lh-180">{{ $review->review }}</p>
                                <span class="static-rating static-rating-sm">
                                @for($i = 0; $i < $review->rating; $i++)
                                        <i class="star fas fa-star voted"></i>
                                    @endfor
                            </span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
@endsection
