<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link rel="icon" href="{{ asset('assets/images/brand/favicon.png') }}" type="image/png">
        <title>{{ config('app.name') }}</title>
    </head>
    <body>
        <div id="admin"></div>
        <link rel="stylesheet" href="{{ asset('assets/css/admin.css') }}">
        <script src="{{ asset('assets/js/admin.js') }}"></script>
    </body>
</html>

