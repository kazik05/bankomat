@extends('front.layouts.master')

@section('meta')
    <title>{{ App\Message::get('404-page:title') }} - {{ config('app.name') }}</title>
@endsection

@section('main')
    <section class="slice slice-lg vh-100 bg-gradient-primary overflow-hidden" data-offset-top="#header-main">
        <div class="bg-absolute-cover vh-100 overflow-hidden">
            <figure class="w-100">
                <img alt="Image placeholder" src="{{ asset('assets/images/svg/backgrounds/bg-4.svg') }}" class="svg-inject">
            </figure>
        </div>
        <div class="container h-100 d-flex align-items-center position-relative zindex-100">
            <div class="col">
                <div class="row justify-content-center">
                    <div class="col-lg-7 text-center">
                        <h6 class="h1 mb-5 font-weight-400 text-white">{{ App\Message::get('404-page:title') }}</h6>
                        <a href="{{ url('/') }}" class="btn btn-white btn-icon rounded-pill hover-translate-y-n3">
                            <span class="btn-inner--icon"><i class="fas fa-home"></i></span>
                            <span class="btn-inner--text">{{ App\Message::get('404-page:return-home') }}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="position-absolute bottom-0 right-4 overflow-hidden">
            <figure class="w-50">
                <img alt="Image placeholder" src="{{ asset('assets/images/svg/illustrations/design-thinking.svg') }}" class="svg-inject opacity-2">
            </figure>
        </div>
    </section>
@endsection
