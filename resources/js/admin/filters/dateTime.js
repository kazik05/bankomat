import moment from 'moment'

moment.locale('ru')

export default function(value) {
    return moment(value).calendar()
}
