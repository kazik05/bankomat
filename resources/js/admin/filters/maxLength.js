export default function(value, max) {
    if (value.length > max) {
        return value.substr(0, max - 3) + '...';
    }
    return value;
}
