import Vue from 'vue'
import { ValidationProvider, ValidationObserver, localize, extend } from 'vee-validate'
import { required, email, confirmed } from 'vee-validate/dist/rules'
import axios from 'axios'

extend('required', {
    ...required,
    message: () => 'Поле обязательно для заполнения'
})
extend('confirmed', {
    ...confirmed,
    message: () => 'Поля не совпадают'
})
extend('email', {
    ...email,
    message: () => 'Не верный формат E-Mail'
})
extend('slug', {
    validate(value) {
        return /^[a-zA-Z0-9\-\/]+$/.test(value)
    },
    message: () => 'В идентификаторе можно использовать только символы латинского алфавита, цифры и тире',
})
extend('is-unique-slug', {
    validate(value, { table, column, ignore }) {
        return new Promise(resolve => {
            axios.post('/api/validate/is-unique-slug', {
                table,
                column,
                ignore,
                value,
            }).then(({ data }) => resolve(data.result))
        })
    },
    params: ['table', 'column', 'ignore'],
    message: () => 'Такой идентификатор уже существует',
})
extend('is-unique', {
    validate(value, { table, column, ignore }) {
        return new Promise(resolve => {
            axios.post('/api/validate/is-unique', {
                table,
                column,
                ignore,
                value,
            }).then(({ data }) => resolve(data.result))
        })
    },
    params: ['table', 'column', 'ignore'],
    message: () => 'Такой ключ уже существует',
})

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)
