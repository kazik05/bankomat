import axios from 'axios'

export default {
    namespaced: true,

    state: () => ({
        user: null,
        token: null,
        tokenExpiresIn: null,
        redirectTo: '/',
    }),

    mutations: {
        SET_USER(state, user) {
            state.user = user
        },
        SET_TOKEN(state, token) {
            state.token = token
            if (token !== null) {
                axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
                if (typeof window !== 'undefined') {
                    localStorage.setItem('token', token)
                }
            } else {
                delete axios.defaults.headers.common['Authorization']
                if (typeof window !== 'undefined') {
                    localStorage.removeItem('token')
                }
            }
        },
        SET_TOKEN_EXPIRES_IN(state, expiresIn) {
            state.tokenExpiresIn = expiresIn
            if (typeof window !== 'undefined') {
                if (expiresIn !== null) {
                    localStorage.setItem('token_expires_in', expiresIn)
                } else {
                    localStorage.removeItem('token_expires_in')
                }
            }
        },
        SET_REDIRECT_TO(state, redirectTo) {
            state.redirectTo = redirectTo
        },
        RESET_REDIRECT_TO(state) {
            state.redirectTo = '/'
        },
    },

    actions: {
        init({ commit, dispatch }) {
            return new Promise(resolve => {
                let token = localStorage.getItem('token')
                let expiresIn = localStorage.getItem('token_expires_in')

                if (token) {
                    commit('SET_TOKEN', token)
                    commit('SET_TOKEN_EXPIRES_IN', expiresIn)
                    dispatch('getUser').then(() => {
                        resolve(true)
                    })
                } else {
                    resolve(false)
                }
            })
        },

        login({commit}, params) {
            return new Promise((resolve, reject) => {
                axios.post('/api/auth/login', params)
                    .then(({data}) => {
                        commit('SET_USER', data.data.user)
                        commit('SET_TOKEN', data.data.token)
                        commit('SET_TOKEN_EXPIRES_IN', data.data.expires_in)

                        resolve(data.data.user)
                    })
                    .catch(reject)
            })
        },

        withAuthorized({commit, state, dispatch}) {
            return new Promise((resolve) => {
                if (state.token !== null && state.tokenExpiresIn <= Math.round(new Date().getTime() / 1000)) {
                    dispatch('refresh').then(resolve)
                } else {
                    resolve(true)
                }
            })
        },

        logout({dispatch, commit}) {
            return new Promise((resolve, reject) => {
                dispatch('withAuthorized').then(() => {
                    axios.post('/api/auth/logout')
                        .then(({data}) => {
                            commit('SET_USER', null)
                            commit('SET_TOKEN', null)
                            commit('SET_TOKEN_EXPIRES_IN', null)

                            resolve(data.data.user)
                        })
                        .catch(() => resolve(false))
                });
            })
        },

        refresh({commit}) {
            return new Promise((resolve, reject) => {
                axios.post('/api/auth/refresh')
                    .then(({data}) => {
                        commit('SET_TOKEN', data.data.token)
                        commit('SET_TOKEN_EXPIRES_IN', data.data.expires_in)

                        resolve(true)
                    })
                    .catch(() => resolve(false))
            })
        },

        getUser({commit, dispatch}) {
            return new Promise((resolve, reject) => {
                dispatch('withAuthorized').then(() => {
                    axios.get('/api/auth/user')
                        .then(({data}) => {
                            commit('SET_USER', data.data)

                            resolve(data.data)
                        })
                        .catch(() => resolve(null))
                });
            })
        },

        hasPermission({ state }, slug) {
            return new Promise((resolve, reject) => {
                if (state.user) {
                    for (let permission of state.user.permissions) {
                        if (permission === slug) {
                            resolve(true)
                        }
                    }
                }
                resolve(false)
            })
        }
    },
}
