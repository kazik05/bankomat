import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import pages from './pages'
import posts from './posts'
import faq from './faq'
import reviews from './reviews'
import settings from './settings'
import partners from './partners'
import messages from './messages'
import paymentSystems from './paymentSystems'
import permissions from './permissions'
import roles from './roles'
import users from './users'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        auth,
        pages,
        posts,
        faq,
        reviews,
        settings,
        partners,
        messages,
        paymentSystems,
        permissions,
        roles,
        users,
    }
})
