import axios from 'axios'

export default {
    namespaced: true,
    state: () => ({
        cache: {},
    }),
    mutations: {
        SET(state, data) {
            for(let key in data) {
                state.cache[key] = data[key]
            }
        },
        SET_VALUES(state, data) {
            for(let key in data) {
                if (typeof state.cache[key] !== 'undefined') {
                    state.cache[key].value = data[key]
                    state.cache[key].updated_at = new Date()
                }
            }
        },
    },
    actions: {
        get({ commit, state, dispatch }, keys) {
            return new Promise(resolve => {
                let result = {}
                let remoteKeys = []
                for(let key of keys) {
                    if (typeof state.cache[key] !== 'undefined') {
                        result[key] = state.cache[key].value
                        result.updated_at = state.cache[key].updated_at
                    } else {
                        remoteKeys.push(key)
                    }
                }
                if (remoteKeys.length) {
                    dispatch('auth/withAuthorized', null, { root: true }).then(() => {
                        axios.post('/api/settings/get', {
                            settings: remoteKeys,
                        }).then(({data}) => {
                            let updated_at = null;
                            for (let item of data.data) {
                                result[item.key] = item.value;
                                updated_at = item.updated_at;
                            }
                            commit('SET', data.data)
                            result.updated_at = updated_at
                            resolve(result)
                        })
                    })
                } else {
                    resolve(result)
                }
            })
        },
        set({ commit, state, dispatch }, data) {
            return new Promise(resolve => {
                dispatch('auth/withAuthorized', null, { root: true }).then(() => {
                    axios.post('/api/settings/set', {settings: data})
                        .then(() => {
                            commit('SET_VALUES', data)
                            resolve(true)
                        })
                })
            })
        },
    },
}
