import axios from 'axios'

export default {
    namespaced: true,
    state: () => ({
        permissions: null,
    }),

    mutations: {
        SET_PERMISSIONS(state, permissions) {
            state.permissions = permissions
        },
    },

    actions: {
        get({ commit, dispatch, state }) {
            return new Promise(resolve => {
                if (state.permissions !== null) {
                    resolve(state.permissions)
                } else {
                    dispatch('auth/withAuthorized', null, {root: true}).then(() => {
                        axios.get('/api/permissions').then(({data}) => {
                            commit('SET_PERMISSIONS', data.data)
                            resolve(data.data)
                        })
                    })
                }
            })
        },
    },
}
