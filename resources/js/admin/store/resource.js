import axios from 'axios'

export default function(resourceName) {
    return {
        namespaced: true,
        state: () => ({
            total: null,
            perPage: null,
            items: {

            },
        }),

        mutations: {
            // Сохранение страницы пагинации
            SET_PAGINATION(state, { page, data, total, perPage }) {
                state.total = total
                state.perPage = perPage
                state.items[page] = data
            },
            // Очистка кэша
            RESET_PAGINATION(state) {
                state.total = null
                state.perPage = null
                state.items = {}
            },
            // Добавление элемента в кэш пагинации
            // Элемент добавляется в первую страницу
            // если такая есть, а затем все элементы
            // сдвигаются вперёд
            ADD_ITEM(state, added) {
                let keys = Object.keys(state.items).sort((a, b) => a < b ? -1 : 1)
                let toRemove = [];
                for(let i = 0; i < keys.length; i++) {
                    if (keys[i] != (i + 1)) {
                        toRemove = keys.splice(i)
                        break;
                    }
                }
                for(let i of toRemove) {
                    delete state.items[i];
                }
                if (keys.length > 0) {
                    let i;
                    for (i of keys) {
                        state.items[i].unshift(added)
                        added = state.items[i].pop()
                    }
                    if (state.items[i].length < state.perPage) {
                        state.items[i].push(added)
                    }
                }
                state.total++
            },
            // Удаление страницы из кэша пагинации
            // Метод удаляет элемент и сдвигает все
            // элементы пагинации
            REMOVE_ITEM(state, removed) {
                // TODO Проверить работу этого метода
                let keys = Object.keys(state.items).sort((a, b) => a < b ? -1 : 1)
                let toRemove = [];
                for(let i = 0; i < keys.length; i++) {
                    if (keys[i] != (i + 1)) {
                        toRemove = keys.splice(i)
                        break;
                    }
                }
                for(let i of toRemove) {
                    delete state.items[i];
                }
                if (keys.length > 0) {
                    for(let pageNum of keys) {
                        let i = 0;
                        let isMoving = false;
                        for(let item of state.items[pageNum]) {
                            if (isMoving) {
                                let next = state.items[pageNum+1];
                                if (typeof next !== 'undefined') {
                                    state.items[pageNum].push(next.pop())
                                } else {
                                    delete state.items[pageNum];
                                }
                            } else if (item.id == removed.id) {
                                state.items[pageNum].splice(i, 1)
                                let next = state.items[pageNum+1];
                                if (typeof next !== 'undefined') {
                                    state.items[pageNum].push(next.pop())
                                } else {
                                    delete state.items[pageNum];
                                }
                            }
                            i++;
                        }
                    }
                }
                state.total--
            },
        },

        actions: {
            page({ commit, state, dispatch }, { page }) {
                return new Promise((resolve) => {
                    if (typeof state.items[page] !== 'undefined') {
                        resolve(state.items[page])
                    } else {
                        dispatch('auth/withAuthorized', null, { root: true }).then(() => {
                            axios.get('/api/' + resourceName, {
                                params: {
                                    page,
                                },
                            }).then(({data}) => {
                                commit('SET_PAGINATION', {
                                    page,
                                    data: data.data,
                                    total: data.meta.total,
                                    perPage: data.meta.per_page,
                                })

                                resolve(data.data)
                            })
                        })
                    }
                })
            },
            create({ commit, state, dispatch }, item) {
                return new Promise((resolve) => {
                    dispatch('auth/withAuthorized', null, { root: true }).then(() => {
                        axios.post('/api/' + resourceName, item)
                            .then(({data}) => {
                                commit('ADD_ITEM', data.data)
                                resolve(data.data)
                            })
                    })
                })
            },
            update({ commit, dispatch }, item) {
                return new Promise((resolve) => {
                    dispatch('auth/withAuthorized', null, { root: true }).then(() => {
                        axios.put('/api/' + resourceName + '/' + item.id, item)
                            .then(({data}) => {
                                commit('RESET_PAGINATION')
                                resolve(data.data)
                            })
                    })
                })
            },
            remove({ commit, dispatch }, item) {
                return new Promise((resolve) => {
                    dispatch('auth/withAuthorized', null, { root: true }).then(() => {
                        axios.delete('/api/' + resourceName + '/' + item.id)
                            .then(() => {
                                commit('REMOVE_ITEM', item)
                                resolve(true)
                            })
                    })
                })
            },
        },
    }
}
