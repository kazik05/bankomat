import axios from 'axios'

export default {
    namespaced: true,
    state: () => ({
        cash: {
            total: null,
            perPage: null,
            items: {

            },
        },
        loan: {
            total: null,
            perPage: null,
            items: {

            },
        },
        card: {
            total: null,
            perPage: null,
            items: {

            },
        },
        refinance: {
            total: null,
            perPage: null,
            items: {

            },
        },
    }),

    mutations: {
        // Сохранение страницы пагинации
        SET_PAGINATION(state, { page, data, total, perPage, type }) {
            state[type].total = total
            state[type].perPage = perPage
            state[type].items[page] = data
        },
        // Очистка кэша
        RESET_PAGINATION(state, type) {
            state[type] = {
                total: null,
                perPage: null,
                items: {

                },
            }
        },
        // Добавление элемента в кэш пагинации
        // Элемент добавляется в первую страницу
        // если такая есть, а затем все элементы
        // сдвигаются вперёд
        ADD_ITEM(state, added) {
            let keys = Object.keys(state[added.type].items).sort((a, b) => a < b ? -1 : 1)
            let toRemove = [];
            for(let i = 0; i < keys.length; i++) {
                if (keys[i] != (i + 1)) {
                    toRemove = keys.splice(i)
                    break;
                }
            }
            for(let i of toRemove) {
                delete state[added.type].items[i];
            }
            if (keys.length > 0) {
                let i;
                for (i of keys) {
                    state[added.type].items[i].unshift(added)
                    added = state[added.type].items[i].pop()
                }
                if (state[added.type].items[i].length < state[added.type].perPage) {
                    state[added.type].items[i].push(added)
                }
            }
            state[added.type].total++
        },
        // Удаление страницы из кэша пагинации
        // Метод удаляет элемент и сдвигает все
        // элементы пагинации
        REMOVE_ITEM(state, removed) {
            // TODO Проверить работу этого метода
            let keys = Object.keys(state[removed.type].items).sort((a, b) => a < b ? -1 : 1)
            let toRemove = [];
            for(let i = 0; i < keys.length; i++) {
                if (keys[i] != (i + 1)) {
                    toRemove = keys.splice(i)
                    break;
                }
            }
            for(let i of toRemove) {
                delete state[removed.type].items[i];
            }
            if (keys.length > 0) {
                for(let pageNum of keys) {
                    let i = 0;
                    let isMoving = false;
                    for(let item of state[removed.type].items[pageNum]) {
                        if (isMoving) {
                            let next = state[removed.type].items[pageNum+1];
                            if (typeof next !== 'undefined') {
                                state[removed.type].items[pageNum].push(next.pop())
                            } else {
                                delete state[removed.type].items[pageNum];
                            }
                        } else if (item.id == removed.id) {
                            state[removed.type].items[pageNum].splice(i, 1)
                            let next = state[removed.type].items[pageNum+1];
                            if (typeof next !== 'undefined') {
                                state[removed.type].items[pageNum].push(next.pop())
                            } else {
                                delete state[removed.type].items[pageNum];
                            }
                        }
                        i++;
                    }
                }
            }
            state[removed.type].total--
        },
    },

    actions: {
        page({ commit, state, dispatch }, { page, type }) {
            return new Promise((resolve) => {
                if (typeof state[type].items[page] !== 'undefined') {
                    resolve(state[type].items[page])
                } else {
                    dispatch('auth/withAuthorized', null, { root: true }).then(() => {
                        axios.get('/api/partners', {
                            params: {
                                page,
                                type,
                            },
                        }).then(({data}) => {
                            commit('SET_PAGINATION', {
                                page,
                                type,
                                data: data.data,
                                total: data.meta.total,
                                perPage: data.meta.per_page,
                            })

                            resolve(data.data)
                        })
                    })
                }
            })
        },
        create({ commit, state, dispatch }, item) {
            return new Promise((resolve) => {
                dispatch('auth/withAuthorized', null, { root: true }).then(() => {
                    axios.post('/api/partners', item)
                        .then(({data}) => {
                            commit('ADD_ITEM', data.data)
                            resolve(data.data)
                        })
                })
            })
        },
        update({ commit, dispatch }, item) {
            return new Promise((resolve) => {
                dispatch('auth/withAuthorized', null, { root: true }).then(() => {
                    axios.put('/api/partners/' + item.id, item)
                        .then(({data}) => {
                            commit('RESET_PAGINATION', 'cash')
                            commit('RESET_PAGINATION', 'loan')
                            commit('RESET_PAGINATION', 'card')
                            commit('RESET_PAGINATION', 'refinance')
                            resolve(data.data)
                        })
                })
            })
        },
        remove({ commit, dispatch }, item) {
            return new Promise((resolve) => {
                dispatch('auth/withAuthorized', null, { root: true }).then(() => {
                    axios.delete('/api/partners/' + item.id)
                        .then(() => {
                            commit('REMOVE_ITEM', item)
                            resolve(true)
                        })
                })
            })
        },
    },
}
