import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import AdminLayout from './layouts/Admin'
import ErrorLayout from './layouts/Error'
import SettingLayout from './layouts/Settings'
import Pages from './views/Pages.vue'
import Posts from './views/Posts.vue'
import Faq from './views/Faq.vue'
import Reviews from './views/Reviews.vue'
import Messages from './views/Messages'
import Partners from './views/Partners'
import PaymentSystems from './views/PaymentSystems'
import Roles from './views/Roles'
import Users from './views/Users'
import Statistics from './views/Statistics'
import SettingsScripts from './views/settings/Scripts'
import SettingsSocial from './views/settings/Social'
import SettingsAPI from './views/settings/API'
import Error403 from './views/errors/403'
import Error404 from './views/errors/404'
import Error500 from './views/errors/500'
import axios from 'axios'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: '/admin/',
    routes: [
        {
            path: '/',
            component: AdminLayout,
            children: [
                {
                    path: '',
                    redirect: { name: 'pages' },
                },
                {
                    path: 'statistics',
                    component: Statistics,
                    name: 'statistics',
                },
                {
                    path: 'roles',
                    component: Roles,
                    name: 'roles',
                },
                {
                    path: 'users',
                    component: Users,
                    name: 'users',
                },
                {
                    path: 'pages',
                    component: Pages,
                    name: 'pages',
                },
                {
                    path: 'messages',
                    component: Messages,
                    name: 'messages',
                },
                {
                    path: 'posts',
                    component: Posts,
                    name: 'posts',
                },
                {
                    path: 'faq',
                    component: Faq,
                    name: 'faq',
                },
                {
                    path: 'reviews',
                    component: Reviews,
                    name: 'reviews',
                },
                {
                    path: 'partners',
                    component: Partners,
                    name: 'partners',
                },
                {
                    path: 'paymentSystems',
                    component: PaymentSystems,
                    name: 'paymentSystems',
                },
                {
                    path: 'settings',
                    component: SettingLayout,
                    children: [
                        {
                            path: '',
                            name: 'settings',
                            redirect: { name: 'settingsScripts' },
                        },
                        {
                            path: 'scripts',
                            name: 'settingsScripts',
                            component: SettingsScripts,
                        },
                        {
                            path: 'social',
                            name: 'settingsSocial',
                            component: SettingsSocial,
                        },
                        {
                            path: 'api',
                            name: 'settingsAPI',
                            component: SettingsAPI,
                        },
                    ],
                },
            ],
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        {
            path: '/errors',
            component: ErrorLayout,
            children: [
                {
                    path: '403',
                    name: 'error403',
                    component: Error403,
                },
                {
                    path: '404',
                    name: 'error404',
                    component: Error404,
                },
                {
                    path: '500',
                    name: 'error500',
                    component: Error500,
                },
            ],
        },
        {
            path: '*',
            redirect: {
                name: 'error404',
            },
        },
    ]
})

axios.interceptors.response.use(
    (response) => {
        return response;
    },
    function (error) {
        if (error.response.status === 401) {
            router.push({ name: 'login' })
        } else if (error.response.status === 403) {
            router.push({ name: 'error403' })
        } else if (error.response.status === 500) {
            router.push({ name: 'error500' })
        } else {
            return Promise.reject(error.response);
        }
    }
);

export default router;
