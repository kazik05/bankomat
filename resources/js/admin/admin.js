import VueQuill from 'vue-quill'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { BootstrapVue } from 'bootstrap-vue'
import VueMeta from 'vue-meta'
import './validate'

Vue.use(VueQuill)
Vue.use(BootstrapVue)
Vue.use(VueMeta, {
    refreshOnceOnNavigation: true
})

Vue.config.productionTip = false

Vue.mixin({
    methods: {
        $permission(slug) {
            if (!this.$store.state.auth.user) {
                return false
            }
            for(let permission of this.$store.state.auth.user.permissions) {
                if (permission === slug) {
                    return true
                }
            }
            return false
        },
    },
})


store.dispatch('auth/init').then(() => {
    new Vue({
        router,
        store,
        render: h => h(App)
    }).$mount('#admin')
})


