import $ from 'jquery'
import 'bootstrap-notify'

class Alert {
    static message(type, message, icon = null) {
        $.notify({message, icon}, {
            type,
            animationIn: 'animated bounceIn',
            animationOut: 'animated bounceOut',
            placement: {
                from: 'bottom',
                align: 'right',
            },
        })
    }

    static success(message) {
        Alert.message('success', message, 'fas fa-check')
    }

    static danger(message) {
        Alert.message('danger', message, 'fas fa-exclamation-triangle')
    }

    static info(message) {
        Alert.message('info', message, 'fas fa-exclamation')
    }

    static dark(message) {
        Alert.message('dark', message, 'fas fa-bomb')
    }

    static unknownError() {
        Alert.danger('Неизвестная ошибка! Зайдите попозже.')
    }
}

export default Alert;
