import plural from 'plural-ru'

export default function months(months) {
    return plural(months, '%d месяц', ' %d месяца', '%d месяцев')
}
