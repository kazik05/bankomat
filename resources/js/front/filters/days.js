import plural from 'plural-ru'

export default function days(term) {
    let years = 0;
    let weeks = 0;
    let days = 0;
    if (term >= 365) {
        years = Math.floor(term / 365);
        term = term - years * 365
    }
    if (term >= 7 && term > 28) {
        weeks = Math.floor(term / 7)
        term = term - weeks * 7
    }
    days = term
    let result = '';
    if (years > 0) {
        result += plural(years, '%d год', ' %d года', '%d лет') + ' '
    }
    if (weeks > 0) {
        result += plural(weeks, '%d неделя', ' %d недели', '%d недель') + ' '
    }
    if (days > 0) {
        result += plural(days, '%d день', ' %d дня', '%d дней') + ' '
    }
    return result.trim()
}
