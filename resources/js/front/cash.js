import Vue from 'vue'
import axios from 'axios'
import Cash from './Cash.vue'

document.addEventListener('DOMContentLoaded', () => {
    let token = document.head.querySelector('meta[name="csrf-token"]');
    axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

    new Vue({
        render: h => h(Cash)
    }).$mount('#script-here')
})
