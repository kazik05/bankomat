import Vue from 'vue'
import axios from 'axios'
import Load from './Loan.vue'

document.addEventListener('DOMContentLoaded', () => {
    let token = document.head.querySelector('meta[name="csrf-token"]');
    axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

    new Vue({
        render: h => h(Load, {
            props: {
                methods: ['yandex_money', 'qiwi', 'webmoney'],
            },
        })
    }).$mount('#script-here')
})
