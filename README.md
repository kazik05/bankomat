## Установка

```
git clone git@bitbucket.org:slexx1234/banking.git .
composer install --no-scripts
cp .env.example .env
php artisan key:generate
php artisan jwt:secret
php artisan storage:link
```

Настраиваем доступы к базе данных в файле `.env` и запускаем.

```
composer update 
php artisan migrate
```

С помощью следующей комманды можно создать администратора:

```
php artisan make:admin email@example.com password
```

Админка находится тут '/admin'. Всего хорошего!
