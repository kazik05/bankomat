<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Slug
{
    public static function normalize($slug)
    {
        return '/' . ltrim(mb_strtolower($slug), '/');
    }

    public static function test($slug)
    {
        return (bool) preg_match('/^[a-zA-Z0-9\-\/]+$/', static::normalize($slug));
    }

    public static function isUnique($table, $column, $slug, $ignore = null)
    {
        $result = DB::table($table)
            ->where($column, static::normalize($slug));

        if ($ignore !== null) {
            $result->where('id', '!=', $ignore);
        }

        return $result->count() === 0;
    }
}
