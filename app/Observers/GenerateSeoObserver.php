<?php

namespace App\Observers;

use App\Generators\RobotsGenerator;
use App\Generators\SitemapGenerator;

class GenerateSeoObserver
{
    public function generate()
    {
        SitemapGenerator::generate();
        RobotsGenerator::generate();
    }

    public function created($model)
    {
        $this->generate();
    }

    public function updated($model)
    {
        $this->generate();
    }

    public function deleted($model)
    {
        $this->generate();
    }
}
