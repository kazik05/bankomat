<?php

namespace App\Observers;

use Cache;

class ResetHtmlCacheObserver
{
    protected function resetCache()
    {
        Cache::store('html')->clear();
    }

    public function created($model)
    {
        $this->resetCache();
    }

    public function updated($model)
    {
        $this->resetCache();
    }

    public function deleted($model)
    {
        $this->resetCache();
    }
}
