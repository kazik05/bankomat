<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasRobots;
    protected $table = 'pages';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'integer',
        'is_system' => 'boolean',
        'is_contentable' => 'boolean',
        'robots_noindex' => 'boolean',
        'robots_nofollow' => 'boolean',
        'details' => 'array',
    ];

    public function getUrlAttribute()
    {
        return url($this->attributes['slug']);
    }
}
