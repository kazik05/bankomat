<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'integer',
        'is_system' => 'boolean',
    ];

    private static $cache = null;

    public static function get($key)
    {
        if (self::$cache == null) {
            self::$cache = self::all();
        }

        foreach(self::$cache as $model) {
            if ($model->key == $key) {
                return $model->message;
            }
        }

        return $key;
    }
}
