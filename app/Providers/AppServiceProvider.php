<?php

namespace App\Providers;

use App\Faq;
use App\Page;
use App\Post;
use App\Review;
use App\Message;
use App\Setting;
use App\Partner;
use App\PaymentSystem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\Observers\GenerateSeoObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Observers\ResetHtmlCacheObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        DB::connection()
            ->getDoctrineSchemaManager()
            ->getDatabasePlatform()
            ->registerDoctrineTypeMapping('enum', 'string');

        Page::observe(GenerateSeoObserver::class);
        Post::observe(GenerateSeoObserver::class);

        Faq::observe(ResetHtmlCacheObserver::class);
        Message::observe(ResetHtmlCacheObserver::class);
        Page::observe(ResetHtmlCacheObserver::class);
        Partner::observe(ResetHtmlCacheObserver::class);
        PaymentSystem::observe(ResetHtmlCacheObserver::class);
        Post::observe(ResetHtmlCacheObserver::class);
        Review::observe(ResetHtmlCacheObserver::class);
        Setting::observe(ResetHtmlCacheObserver::class);

        Schema::defaultStringLength(191);

        if (preg_match('/^https/i', config('app.url'))) {
            URL::forceScheme('https');
        }
    }
}
