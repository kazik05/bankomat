<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasRobots;

    protected $table = 'posts';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'integer',
        'robots_noindex' => 'boolean',
        'robots_nofollow' => 'boolean',
    ];

    public function getUrlAttribute()
    {
        return url('/blog/' . ltrim($this->attributes['slug'], '/'));
    }
}
