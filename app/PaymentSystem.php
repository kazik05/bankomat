<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentSystem extends Model
{
    protected $table = 'payment_systems';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'integer',
    ];

    public function partners()
    {
        return $this->belongsToMany(Partner::class, 'partner_payment_systems');
    }
}
