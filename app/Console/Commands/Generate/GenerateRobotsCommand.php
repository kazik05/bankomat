<?php

namespace App\Console\Commands\Generate;

use Illuminate\Console\Command;
use App\Generators\RobotsGenerator;

class GenerateRobotsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:robots';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate robots.txt file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        RobotsGenerator::generate();
        $this->info('Robots generated!');
    }
}
