<?php

namespace App\Console\Commands\Api;

use App\Parsers\AdmitadComParser;
use Illuminate\Console\Command;

class ApiBalanceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:balance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update balance command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->updateAdmitadComBalance();
        $this->updateLeadsSuBalance();
    }

    public function updateAdmitadComBalance()
    {
        $parser = new AdmitadComParser();
        $parser->updateBalance();
    }

    public function updateLeadsSuBalance()
    {

    }
}
