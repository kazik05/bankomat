<?php

namespace App\Console\Commands\Api;

use App\Partner;
use App\Parsers\LeadsSuParser;
use Illuminate\Console\Command;
use App\Parsers\AdmitadComParser;

class ApiStatusesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:statuses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update partners statuses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->disablePartnersWithoutLinks();
        $this->updateLeadsSuPartners();
        $this->updateAdmitadComPartners();
    }

    public function disablePartnersWithoutLinks()
    {
        Partner::query()
            ->where('link', '=', null)
            ->update([
                'is_active' => false,
            ]);
    }

    public function updateLeadsSuPartners()
    {
        $parser = new LeadsSuParser();
        $parser->updateStatuses();
    }

    public function updateAdmitadComPartners()
    {
        $parser = new AdmitadComParser();
        $parser->updateStatuses();
    }
}
