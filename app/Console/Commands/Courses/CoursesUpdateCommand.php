<?php

namespace App\Console\Commands\Courses;

use App\Course;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CoursesUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'courses:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update courses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $result = @file_get_contents('http://www.cbr.ru/scripts/XML_daily.asp');
        $xml = new \SimpleXMLElement($result);
        foreach($xml->children() as $child) {
            $id = (string) $child->attributes()['ID'];
            $course = Course::where('cbr_id', $id)->first();
            $nominal = (integer) $child->Nominal;
            if (!$course) {
                $course = new Course;
                $course->cbr_id = $id;
            }
            if ($nominal !== $course->nominal) {
                $course->updated_at = Carbon::now();
            }
            $course->num_code = (string) $child->NumCode;
            $course->char_code = (string) $child->CharCode;
            $course->nominal = (integer) $child->Nominal;
            $course->name = (string) $child->Name;
            $course->value = (float) str_replace(',', '.', $child->Value);
            $course->save();
        }
    }
}
