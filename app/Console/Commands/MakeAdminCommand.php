<?php

namespace App\Console\Commands;

use DB;
use App\Role;
use App\User;
use Illuminate\Console\Command;

class MakeAdminCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:admin {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make administrator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::create([
            'name' => 'Admin',
            'email' => $this->argument('email'),
            'password' => bcrypt($this->argument('password')),
        ]);
        $role = Role::query()->where('slug', 'admin')->firstOrFail();

        DB::table('user_roles')->insert([
            'role_id' => $role->id,
            'user_id' => $user->id,
        ]);
        $this->info('Admin was created!');
    }
}
