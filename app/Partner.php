<?php

namespace App;

use EndyJasmi\Cuid;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $table = 'partners';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'integer',
        'min' => 'integer',
        'max' => 'integer',
        'rate' => 'float',
        'mix_age' => 'integer',
        'max_age' => 'integer',
        'min_term' => 'integer',
        'max_term' => 'integer',
        'grace_period' => 'integer',
        'is_active' => 'boolean',
        'external_id' => 'integer',
    ];

    const TYPE_CASH = 'cash';
    const TYPE_LOAN = 'loan';
    const TYPE_CARD = 'card';
    const TYPE_REFINANCE = 'refinance';

    public static function boot()
    {
        parent::boot();
        self::creating(function($model) {
            $model->hash = self::generateHash();
            if ($model->link) {
                $model->domain = parse_url($model->link, PHP_URL_HOST);
            }
        });
        self::updating(function($model) {
            if ($model->link) {
                $model->domain = parse_url($model->link, PHP_URL_HOST);
            }
        });
    }

    static function generateHash()
    {
        while(true) {
            $hash = Cuid::slug();
            if (self::query()->where('hash', $hash)->count() === 0) {
                return $hash;
            }
        }
    }

    public function clicks()
    {
        return $this->hasMany(Click::class);
    }

    public function getUrlAttribute()
    {
        return url('c/' . $this->attributes['hash']);
    }

    public function paymentSystems()
    {
        return $this->belongsToMany(PaymentSystem::class, 'partner_payment_systems');
    }
}
