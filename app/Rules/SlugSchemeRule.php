<?php

namespace App\Rules;

use App\Slug;
use Illuminate\Contracts\Validation\Rule;

class SlugSchemeRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Slug::test($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'В идентификаторе можно использовать только символы латинского алфавита, цифры и тире';
    }
}
