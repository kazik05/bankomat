<?php

namespace App\Rules;

use App\Slug;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Validation\Rule;

class SlugUniqueRule implements Rule
{
    protected $table;
    protected $column;
    protected $ignore;

    /**
     * SlugUniqueRule constructor.
     * @param $table
     * @param $column
     * @param null $ignore
     */
    public function __construct($table, $column, $ignore = null)
    {
        $this->table = $table;
        $this->column = $column;
        $this->ignore = $ignore;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $result = DB::table($this->table)
            ->where($this->column, Slug::normalize($value));

        if ($this->ignore) {
            $result->where('id', '!=', $this->ignore);
        }

        return $result->count() === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Идентификатор должен быть уникальным';
    }
}
