<?php

namespace App;

trait HasRobots
{
    public function getRobotsAttribute()
    {
        if ($this->attributes['robots_noindex'] && $this->attributes['robots_nofollow']) {
            return 'none';
        }

        if ($this->attributes['robots_noindex']) {
            return 'noindex';
        }

        if ($this->attributes['robots_nofollow']) {
            return 'nofollow';
        }

        return 'all';
    }
}
