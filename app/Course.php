<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';
    protected $guarded = ['id'];
    public $timestamps = false;
    protected $casts = [
        'id' => 'integer',
        'nominal' => 'integer',
        'value' => 'float',
    ];
    protected $dates = ['updated_at'];
}
