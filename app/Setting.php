<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'integer',
    ];

    private static $cache = null;

    public static function set($key, $value)
    {
        $model = self::query()
            ->where('key', $key)
            ->first();

        if (!$model) {
            $model = new Setting;
            $model->key = $key;
        }

        $model->value = $value;
        $model->save();
    }

    public static function get($key, $default = null)
    {

        if (self::$cache == null) {
            self::$cache = self::all();
        }

        foreach(self::$cache as $model) {
            if ($model->key == $key) {
                return $model->value;
            }
        }

        return $default;
    }
}
