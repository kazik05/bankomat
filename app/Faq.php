<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table = 'faq';
    protected $casts = [
        'id' => 'integer',
    ];
    protected $guarded = ['id'];
}
