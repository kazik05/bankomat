<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $guarded = ['id'];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles');
    }

    public function hasPermission($slug)
    {
        foreach($this->roles as $role) {
            foreach($role->permissions as $permission) {
                if ($permission->slug == $slug) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return int|mixed
     */
    public function getLevelAttribute()
    {
        $level = 0;
        foreach($this->roles as $role) {
            if ($role->level > $level) {
                $level = $role->level;
            }
        }
        return $level;
    }
}
