<?php

namespace App\Parsers;

abstract class AbstractParser
{
    abstract public function updateStatuses();
}
