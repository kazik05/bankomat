<?php

namespace App\Parsers;

use App\Partner;
use App\Setting;
use GuzzleHttp\Client;

class AdmitadComParser extends AbstractParser
{
    private $accessToken = null;
    private $httpClient = null;

    public function __construct()
    {
        $this->httpClient = new Client([
            'timeout'  => 5.0,
        ]);
    }

    public function getAccessToken()
    {
        if ($this->accessToken === null && Setting::get('admitad_com_id') !== null && Setting::get('admitad_com_secret')) {
            $response = $this->httpClient->post('https://api.admitad.com/token/', [
                'form_params' => [
                    'client_id' => Setting::get('admitad_com_id'),
                    'scope' => 'advcampaigns websites private_data_balance',
                    'grant_type' => 'client_credentials',
                ],
                'headers' => [
                    'Authorization' => 'Basic ' . base64_encode(Setting::get('admitad_com_id') . ':' . Setting::get('admitad_com_secret')),
                ],
            ]);
            $body = json_decode((string) $response->getBody(), true);
            $this->accessToken = $body['access_token'];
        }

        return $this->accessToken;
    }

    public function getRequest($url, $query = [])
    {
        $accessToken = $this->getAccessToken();
        if ($accessToken === null) {
            return null;
        }
        return @json_decode((string) $this->httpClient->get($url, [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken,
            ],
            'query' => $query,
        ])->getBody(), true);
    }

    public function getWebsites()
    {
        return $this->getRequest('https://api.admitad.com/websites/v2/');
    }

    public function getOffer($id)
    {
        return $this->getRequest('https://api.admitad.com/advcampaigns/' . $id . '/');
    }

    public function getBalance()
    {
        return $this->getRequest('https://api.admitad.com/me/balance/extended/');
    }

    public function updateBalance()
    {
        $result = $this->getBalance();
        dd($result);
    }

    public function updateStatuses()
    {
        Partner::query()
            ->where('domain', 'ad.admitad.com')
            ->where('external_id', '=', null)
            ->update([
                'is_active' => false,
            ]);

        $partners = Partner::query()
            ->where('domain', 'ad.admitad.com')
            ->where('external_id', '!=', null)
            ->get();

        foreach($partners as $partner) {
            $offer = null;

            try {
                $offer = $this->getOffer($partner->external_id);
            } catch (\Exception $e) {}

            $partner->is_active = $offer && $offer['status'] === 'active';
            $partner->save();
        }
    }
}
