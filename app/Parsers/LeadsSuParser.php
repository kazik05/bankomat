<?php

namespace App\Parsers;

use App\Partner;
use App\Setting;

class LeadsSuParser extends AbstractParser
{
    public function token()
    {
        return Setting::get('leads_su_token');
    }

    public function updateStatuses()
    {
        if (!$this->token()) {
            return;
        }

        $result = @json_decode(file_get_contents('http://api.leads.su/webmaster/offers/connectedPlatforms?token=' . $this->token() . '&limit=500'), false);
        $externalIds = array_map(function($result) {
            return (int) $result->id;
        }, $result->data);

        Partner::query()
            ->where('domain', 'pxl.leads.su')
            ->whereIn('external_id', $externalIds)
            ->update([
                'is_active' => true,
            ]);

        Partner::query()
            ->where('domain', 'pxl.leads.su')
            ->whereNotIn('external_id', $externalIds)
            ->update([
                'is_active' => false,
            ]);

        Partner::query()
            ->where('domain', 'pxl.leads.su')
            ->where('external_id', null)
            ->update([
                'is_active' => false,
            ]);
    }
}
