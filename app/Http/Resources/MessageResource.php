<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'key' => $this->resource->key,
            'message' => $this->resource->message,
            'created_at' => $this->resource->created_at,
            'updated_at' => $this->resource->updated_at,
            'is_system' => $this->resource->is_system,
        ];
    }
}
