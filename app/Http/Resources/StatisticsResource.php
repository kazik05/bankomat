<?php

namespace App\Http\Resources;

use App\Click;
use Illuminate\Http\Resources\Json\ResourceCollection;

class StatisticsResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function(Click $click) {
                return [
                    'id' => $click->id,
                    'ip' => $click->ip,
                    'partner' => $click->partner ? new PartnerResource($click->partner) : null,
                    'country' => $click->country ? new CountryResource($click->country) : null,
                    'city' => $click->city ? new CityResource($click->city) : null,
                    'user_agent' => $click->user_agent,
                    'referer' => $click->referer,
                    'referer_domain' => $click->referer_domain,
                    'is_bot' => $click->is_bot,
                    'bot' => $click->bot,
                    'os' => $click->os,
                    'client_type' => $click->client_type,
                    'client_name' => $click->client_name,
                    'client_version' => $click->client_version,
                    'device_type' => $click->device_type,
                    'created_at' => $click->created_at,
                    'updated_at' => $click->updated_at,
                ];
            }),
        ];
    }
}
