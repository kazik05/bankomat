<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'slug' => $this->resource->slug,
            'title' => $this->resource->title,
            'description' => $this->resource->description,
            'seo_title' => $this->resource->seo_title,
            'seo_description' => $this->resource->seo_description,
            'seo_keywords' => $this->resource->seo_keywords,
            'content' => $this->resource->content,
            'image' => $this->resource->image,
            'created_at' => $this->resource->created_at,
            'updated_at' => $this->resource->updated_at,
            'robots_noindex' => $this->resource->robots_noindex,
            'robots_nofollow' => $this->resource->robots_nofollow,
        ];
    }
}
