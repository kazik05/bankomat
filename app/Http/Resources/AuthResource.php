<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthResource extends JsonResource
{
    protected $token;

    /**
     * AuthResource constructor.
     * @param User $resource
     * @param string $token
     */
    public function __construct(User $resource, string $token)
    {
        parent::__construct($resource);
        $this->token = $token;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'token' => $this->token,
            'expires_in' => config('jwt.ttl') * 60 + time(),
            'user' => new UserResource($this->resource),
        ];
    }
}
