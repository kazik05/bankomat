<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $permissions = [];
        foreach($this->resource->roles as $role) {
            foreach($role->permissions as $permission) {
                if (!in_array($permission->slug, $permissions)) {
                    $permissions[] = $permission->slug;
                }
            }
        }
        return [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'email' => $this->resource->email,
            'roles' => $this->resource->roles()->get(['roles.id'])->pluck('id'),
            'permissions' => $permissions,
            'level' => $this->resource->level,
            'created_at' => $this->resource->created_at,
            'updated_at' => $this->resource->updated_at,
        ];
    }
}
