<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PartnerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'type' => $this->resource->type,
            'grace_period' => $this->resource->grace_period,
            'name' => $this->resource->name,
            'image' => $this->resource->image,
            'rate' => $this->resource->rate,
            'min' => $this->resource->min,
            'max' => $this->resource->max,
            'min_age' => $this->resource->min_age,
            'max_age' => $this->resource->max_age,
            'link' => $this->resource->link,
            'hash' => $this->resource->hash,
            'clicks_counter' => $this->resource->clicks_counter ?? 0,
            'created_at' => $this->resource->created_at,
            'updated_at' => $this->resource->updated_at,
            'min_term' => $this->resource->min_term,
            'max_term' => $this->resource->max_term,
            'is_active' => $this->resource->is_active,
            'external_id' => $this->resource->external_id,
            'domain' => $this->resource->domain,
            'payment_systems' => $this->resource->paymentSystems()->get(['payment_systems.id'])->pluck('id'),
        ];
    }
}
