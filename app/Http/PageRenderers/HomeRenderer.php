<?php

namespace App\Http\PageRenderers;

use App\Faq;
use App\Page;
use App\Post;
use App\Review;
use App\Partner;

class HomeRenderer extends AbstractPageRenderer
{
    public function render()
    {
        return view('front.pages.home', [
            'page' => $this->page,
            'reviews' => Review::query()
                ->inRandomOrder()
                ->limit(7)
                ->get(),
            'posts' => Post::query()
                ->inRandomOrder()
                ->limit(3)
                ->get(),
            'faq' => Faq::query()
                ->orderBy('id', 'asc')
                ->inRandomOrder()
                ->limit(6)
                ->get(),
        ]);
    }
}
