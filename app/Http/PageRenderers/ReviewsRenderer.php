<?php

namespace App\Http\PageRenderers;

use App\Review;

class ReviewsRenderer extends AbstractPageRenderer
{
    public function render()
    {
        return view('front.pages.reviews', [
            'page' => $this->page,
            'reviews' => Review::query()
                ->orderBy('id', 'desc')
                ->get(),
        ]);
    }
}
