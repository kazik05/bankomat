<?php

namespace App\Http\PageRenderers;

class SimpleRenderer extends AbstractPageRenderer
{
    public function render()
    {
        return view('front.pages.simple', [
            'page' => $this->page,
        ]);
    }
}
