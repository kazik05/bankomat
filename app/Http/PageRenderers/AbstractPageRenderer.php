<?php

namespace App\Http\PageRenderers;

use App\Page;

abstract class AbstractPageRenderer
{
    protected $page;

    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    abstract public function render();
}
