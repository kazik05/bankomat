<?php

namespace App\Http\PageRenderers;

use App\Course;

class CoursesRenderer extends AbstractPageRenderer
{
    public function render()
    {
        return view('front.pages.courses', [
            'page' => $this->page,
            'courses' => Course::all(),
        ]);
    }
}
