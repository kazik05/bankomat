<?php

namespace App\Http\PageRenderers;

use App\Post;

class BlogRenderer extends AbstractPageRenderer
{
    public function render()
    {
        return view('front.pages.blog', [
            'page' => $this->page,
            'posts' => Post::query()
                ->orderBy('id', 'desc')
                ->paginate(9),
        ]);
    }
}
