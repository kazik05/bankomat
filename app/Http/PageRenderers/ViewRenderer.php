<?php

namespace App\Http\PageRenderers;

use App\Partner;

class ViewRenderer extends AbstractPageRenderer
{
    public function render()
    {
        return view($this->page->details['view'], [
            'page' => $this->page,
        ]);
    }
}
