<?php

namespace App\Http\PageRenderers;

use App\Partner;

class ScriptRenderer extends AbstractPageRenderer
{
    public function render()
    {
        return view('front.pages.script', [
            'page' => $this->page,
        ]);
    }
}
