<?php

namespace App\Http\PageRenderers;

use App\Faq;

class FaqRenderer extends AbstractPageRenderer
{
    public function render()
    {
        return view('front.pages.faq', [
            'page' => $this->page,
            'faqs' => Faq::query()
                ->orderBy('id', 'desc')
                ->get(),
        ]);
    }
}
