<?php

namespace App\Http\Middleware;

use Cache;
use Closure;
use Illuminate\Contracts\Auth\Guard;

class HtmlCache
{
    const CACHING_TIME = 600; // Minutes
    const REPLACE_CSRF_TOKEN = true;
    const CSRF_TOKEN_TAG = '<csrf-token-here>';

    protected function cache()
    {
        return Cache::store('html');
    }

    /**
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function handle($request, Closure $next)
    {
        if (config('app.env') !== 'production') {
            return $next($request);
        }

        $key = $request->fullUrl();

        if ($this->cache()->has($key)) {
            $content = $this->cache()->get($key);
            if (self::REPLACE_CSRF_TOKEN) {
                $content = str_replace(self::CSRF_TOKEN_TAG, csrf_token(), $content);
            }
            return response($content);
        }

        $response = $next($request);
        $content = $response->getContent();
        if (self::REPLACE_CSRF_TOKEN) {
            $content = str_replace(csrf_token(), self::CSRF_TOKEN_TAG, $content);
        }
        $this->cache()->put($key, $content, self::CACHING_TIME);

        return $response;
    }
}
