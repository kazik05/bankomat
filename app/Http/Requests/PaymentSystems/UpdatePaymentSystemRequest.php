<?php

namespace App\Http\Requests\PaymentSystems;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePaymentSystemRequest extends StorePaymentSystemRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $parentRules = parent::rules();

        $parentRules['slug'] = ['required', 'string', Rule::unique('payment_systems', 'slug')->ignore($this->route('payment_system')->id)];

        return $parentRules;
    }
}
