<?php

namespace App\Http\Requests\PaymentSystems;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StorePaymentSystemRequest
 * @package App\Http\Requests\PaymentSystems
 * @property-read string $slug
 * @property-read string $name
 */
class StorePaymentSystemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => 'required|string|unique:payment_systems,slug',
            'name' => 'required|string',
        ];
    }
}
