<?php

namespace App\Http\Requests\Messages;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateMessageRequest extends StoreMessageRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $parentRules = parent::rules();

        $parentRules['key'] = ['required', 'string', 'max:255', Rule::unique('messages', 'key')->ignore($this->route('message')->id)];

        return $parentRules;
    }
}
