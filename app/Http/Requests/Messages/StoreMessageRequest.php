<?php

namespace App\Http\Requests\Messages;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreMessageRequest
 * @package App\Http\Requests
 * @property-read string $key
 * @property-read string $message
 */
class StoreMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key' => 'required|string|max:255|unique:messages,key',
            'message' => 'required|string|max:255',
        ];
    }
}
