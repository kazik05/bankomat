<?php

namespace App\Http\Requests\Users;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateUserRequest
 * @package App\Http\Requests\Users
 * @property-read string|null $password
 */
class UpdateUserRequest extends StoreUserRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $parentRules = parent::rules();

        $parentRules['email'] = ['required', 'string', 'email', Rule::unique('users', 'email')->ignore($this->route('user')->id)];
        $parentRules['password'] = 'nullable|string';

        return $parentRules;
    }
}
