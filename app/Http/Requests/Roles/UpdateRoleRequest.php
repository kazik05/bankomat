<?php

namespace App\Http\Requests\Roles;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRoleRequest extends StoreRoleRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $parentRules = parent::rules();

        $parentRules['slug'] = ['required', 'string', Rule::unique('roles', 'slug')->ignore($this->route('role')->id)];

        return $parentRules;
    }
}
