<?php

namespace App\Http\Requests\Roles;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRoleRequest
 * @package App\Http\Requests\Roles
 * @property-read string $slug
 * @property-read string $name
 * @property-read int $level
 * @property-read int[]|null $permissions
 */
class StoreRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => 'required|string|unique:roles,slug',
            'name' => 'required|string',
            'level' => 'required|integer',
            'permissions' => 'array',
            'permissions.*' => 'integer|exists:permissions,id',
        ];
    }
}
