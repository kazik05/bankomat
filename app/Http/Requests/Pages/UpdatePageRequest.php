<?php

namespace App\Http\Requests\Pages;

use App\Rules\SlugSchemeRule;
use App\Rules\SlugUniqueRule;
use App\Http\Request\Pages\StorePageRequest;

class UpdatePageRequest extends StorePageRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $parentRules = parent::rules();

        $parentRules['slug'] = ['required', 'string', new SlugSchemeRule(), new SlugUniqueRule('pages', 'slug', $this->route('page')->id)];

        return $parentRules;
    }
}
