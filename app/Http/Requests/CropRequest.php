<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Запрос на загрузку фотографии к последующему кадрированию
 * @property-read int $type - Тип изображения avatar 100x100 или post 800x600
 * @property-read int $height - Изображение какой высоты вырезать из исходного
 * @property-read int $width - Изображение какой ширины вырезать из исходного
 * @property-read int $x - X координата с которой начинается обрезка изображения
 * @property-read int $y - Y координата с которой начинается обрезка изображения
 * @property-read \Illuminate\Http\UploadedFile $image - Исходное изображение для обрезки
 */
class CropRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|string|in:avatar,post,partner,card',
            'height' => 'required|integer|min:0',
            'width' => 'required|integer|min:0',
            'x' => 'required|integer',
            'y' => 'required|integer',
            'image' => 'required|file|mimetypes:image/jpeg,image/png|max:2048'
        ];
    }
}
