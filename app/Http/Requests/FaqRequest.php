<?php

namespace App\Http\Requests;

use App\Lang;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FaqRequest
 * @package App\Http\Requests
 * @property-read string $question
 * @property-read string $answer
 * @property-read string $icon
 */
class FaqRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question' => 'required|string|max:255',
            'answer' => 'required|string',
            'icon' => 'required|string',
        ];
    }
}
