<?php

namespace App\Http\Requests;

use App\Partner;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class PartnerRequest
 * @package App\Http\Requests
 * @property-read string $type
 * @property-read string $name
 * @property-read string $image
 * @property-read float $rate
 * @property-read null|int $min
 * @property-read null|int $max
 * @property-read null|int $min_age
 * @property-read null|int $max_age
 * @property-read null|int $min_term
 * @property-read null|int $max_term
 * @property-read string|null $link
 * @property-read null|int $grace_period
 * @property-read int[] $payment_systems
 * @property-read bool $is_active
 * @property-read null|int $external_id
 */
class PartnerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ['required', 'string', Rule::in([Partner::TYPE_REFINANCE, Partner::TYPE_LOAN, Partner::TYPE_CASH, Partner::TYPE_CARD])],
            'grace_period' => 'nullable|integer|min:0',
            'name' => 'required|string',
            'image' => 'required|string',
            'rate' => 'required|numeric|between:0.00,99.99',
            'min' => 'nullable|integer|min:0',
            'max' => 'nullable|integer|min:0',
            'min_age' => 'nullable|integer|min:0',
            'max_age' => 'nullable|integer|min:0',
            'link' => 'nullable|string',
            'min_term' => 'nullable|integer|min:0',
            'max_term' => 'nullable|integer|min:0',
            'payment_systems' => 'array',
            'payment_systems.*' => 'integer|exists:payment_systems,id',
            'is_active' => 'required|boolean',
            'external_id' => 'nullable|integer|min:0',
        ];
    }
}
