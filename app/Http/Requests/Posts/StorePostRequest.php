<?php

namespace App\Http\Request\Posts;

use App\Rules\SlugSchemeRule;
use App\Rules\SlugUniqueRule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StorePostRequest
 * @package App\Http\Requests
 * @property-read string $slug
 * @property-read string $title
 * @property-read string|null $description
 * @property-read string|null $seo_title
 * @property-read string|null $seo_description
 * @property-read string|null $seo_keywords
 * @property-read string $content
 * @property-read string $image
 * @property-read boolean $robots_noindex
 * @property-read boolean $robots_nofollow
 */
class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|string',
            'slug' => ['required', 'string', new SlugSchemeRule(), new SlugUniqueRule('posts', 'slug')],
            'title' => 'required|string|max:255',
            'description' => 'nullable|string',
            'seo_title' => 'nullable|string|max:255',
            'seo_description' => 'nullable|string|max:255',
            'seo_keywords' => 'nullable|string|max:255',
            'content' => 'nullable|string',
            'robots_noindex' => 'required|boolean',
            'robots_nofollow' => 'required|boolean',
        ];
    }
}
