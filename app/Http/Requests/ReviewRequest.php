<?php

namespace App\Http\Requests;

use App\Lang;
use App\Review;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ReviewRequest
 * @package App\Http\Requests
 * @property-read string $name
 * @property-read string $review
 * @property-read string $image
 * @property-read integer $rating
 */
class ReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'review' => 'required|string',
            'image' => 'required|string',
            'rating' => 'required|integer|min:1|max:5',
        ];
    }
}
