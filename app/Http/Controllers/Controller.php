<?php

namespace App\Http\Controllers;

use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param array|string $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function success($data)
    {
        if (is_string($data)) {
            return response()->json([
                'status' => 'success',
                'message' => $data,
            ]);
        }

        return response()->json([
            'status' => 'success',
            'data' => $data,
        ]);
    }

    /**
     * @param int $status
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    protected function error(int $status, string $message)
    {
        return response()->json([
            'status' => 'error',
            'message' => $message
        ], $status);
    }

    protected function permission($slug)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {}

        if (!JWTAuth::user()) {
            abort(401);
        }
        if (!JWTAuth::user()->hasPermission($slug)) {
            abort(403);
        }
    }
}
