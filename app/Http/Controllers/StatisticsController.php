<?php

namespace App\Http\Controllers;

use App\City;
use App\Click;
use App\Country;
use App\Partner;
use GeoIp2\Database\Reader;
use DeviceDetector\DeviceDetector;
use Illuminate\Support\Facades\Request;
use DeviceDetector\Parser\Device\DeviceParserAbstract;

class StatisticsController extends Controller
{
    /**
     * @param $hash
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \GeoIp2\Exception\AddressNotFoundException
     * @throws \MaxMind\Db\Reader\InvalidDatabaseException
     */
    public function click($hash)
    {
        $partner = Partner::query()
            ->where('hash', $hash)
            ->firstOrFail();

        $click = new Click;
        $click->partner_id = $partner->id;

        $ip = Request::ip();
        if (!empty($ip)) {
            $click->ip = $ip;
            try {
                $reader = new Reader(base_path('vendor/lysenkobv/maxmind-geolite2-database/city.mmdb'));
                $record = $reader->city($ip);
                if ($record->country->geonameId !== null) {
                    $country = Country::query()
                        ->where('geoname_id', $record->country->geonameId)
                        ->first();
                    if (!$country) {
                        $country = new Country;
                        $country->geoname_id = $record->country->geonameId;
                        $country->code = $record->country->isoCode;
                        $country->name = $record->country->names['ru'];
                        $country->save();
                    }
                    $click->country_id = $country->id;
                    if ($record->city->geonameId !== null) {
                        $city = City::query()
                            ->where('geoname_id', $record->city->geonameId)
                            ->first();
                        if (!$city) {
                            $city = new City;
                            $city->geoname_id = $record->city->geonameId;
                            $city->name = $record->city->names['ru'];
                            $city->country_id = $country->id;
                            $city->save();
                        }
                        $click->city_id = $city->id;
                    }
                }
            } catch(\Exception $e) {}
        }

        $referer = Request::server('HTTP_REFERER');
        if (!empty($referer)) {
            $click->referer = $referer;
            $click->referer_domain = parse_url($referer, PHP_URL_HOST);
        }

        $ua = Request::server('HTTP_USER_AGENT');
        if (!empty($ua)) {
            $click->user_agent = $ua;
            $dd = new DeviceDetector($ua);
            $dd->parse();
            if ($dd->isBot()) {
                $click->is_bot = true;
                $click->bot = $dd->getBot();
            } else {
                $click->os = $dd->getOs()['name'] ?? null;
                $client = $dd->getClient();
                $click->client_type = $client['type'];
                $click->client_name = $client['name'];
                $click->client_version = $client['version'];
                $click->device_type = $dd->getDeviceName();
            }
        }

        $click->save();

        $partner->clicks_counter++;
        $partner->save();

        return redirect($partner->link);
    }
}
