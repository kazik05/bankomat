<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LangRequest;
use App\Http\Requests\ReviewRequest;
use App\Http\Resources\ReviewResource;
use App\Lang;
use App\Review;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ReviewsController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $this->permission('reviews:browse');

        return ReviewResource::collection(Review::query()
            ->orderBy('created_at', 'desc')
            ->paginate(50));
    }

    /**
     * @param ReviewRequest $request
     * @return ReviewResource
     */
    public function store(ReviewRequest $request)
    {
        $this->permission('reviews:create');

        $review = new Review;
        $review->name = $request->name;
        $review->review = $request->review;
        $review->image = $request->image;
        $review->rating = $request->rating;
        $review->save();

        return new ReviewResource($review);
    }

    /**
     * @param Review $review
     * @return ReviewResource
     */
    public function show(Review $review)
    {
        $this->permission('reviews:browse');

        return new ReviewResource($review);
    }

    /**
     * @param ReviewRequest $request
     * @param Review $review
     * @return ReviewResource
     */
    public function update(ReviewRequest $request, Review $review)
    {
        $this->permission('reviews:update');

        $review->name = $request->name;
        $review->review = $request->review;
        $review->image = $request->image;
        $review->rating = $request->rating;
        $review->save();

        return new ReviewResource($review);
    }

    /**
     * @param Review $review
     * @throws \Exception
     */
    public function destroy(Review $review)
    {
        $this->permission('reviews:delete');

        $review->delete();
    }
}
