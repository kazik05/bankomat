<?php

namespace App\Http\Controllers\Api;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Resources\UserResource;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\StoreUserRequest;
use App\Http\Requests\Roles\UpdateRoleRequest;
use App\Http\Requests\Users\UpdateUserRequest;

class UsersController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $this->permission('users:browse');

        return UserResource::collection(User::query()
            ->orderBy('created_at', 'desc')
            ->paginate(50));
    }

    /**
     * @param StoreUserRequest $request
     * @return UserResource
     */
    public function store(StoreUserRequest $request)
    {
        $this->permission('users:create');

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        if (JWTAuth::user()->hasPermission('roles:browse') && is_array($request->roles)) {
            $pivot = [];
            // Можно ставить роли только с уровнем меньше чем у текущего пользователя
            foreach($request->roles as $roleId) {
                if (Role::find($roleId)->level < JWTAuth::user()->level) {
                    $pivot[] = [
                        'role_id' => $roleId,
                        'user_id' => $user->id,
                    ];
                }
            }
            DB::table('user_roles')->insert($pivot);
        }

        return new UserResource($user);
    }

    /**
     * @param User $user
     * @return UserResource
     */
    public function show(User $user)
    {
        $this->permission('users:browse');
        return new UserResource($user);
    }

    /**
     * @param UpdateRoleRequest $request
     * @param User $user
     * @return UserResource
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $this->permission('users:update');

        // Можно изменять пользователей только с меньшим уровнем
        if ($user->level >= JWTAuth::user()->level) {
            abort(403);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->has('password')) {
            $user->password = bcrypt($request->password);
        }
        $user->save();

        if (JWTAuth::user()->hasPermission('roles:browse') && is_array($request->roles)) {
            DB::table('user_roles')->where('user_id', $user->id)->delete();
            $pivot = [];
            // Можно ставить роли только с уровнем меньше чем у текущего пользователя
            foreach($request->roles as $roleId) {
                if (Role::find($roleId)->level < JWTAuth::user()->level) {
                    $pivot[] = [
                        'role_id' => $roleId,
                        'user_id' => $user->id,
                    ];
                }
            }
            DB::table('user_roles')->insert($pivot);
        }

        return new UserResource($user);
    }

    /**
     * @param User $user
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $this->permission('users:delete');

        // Можно удалять пользователей только с меньшим уровнем
        if ($user->level >= JWTAuth::user()->level) {
            abort(403);
        }

        $user->delete();
    }
}
