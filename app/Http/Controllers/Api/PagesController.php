<?php

namespace App\Http\Controllers\Api;

use App\Page;
use App\Slug;
use App\Http\Resources\PageResource;
use App\Http\Controllers\Controller;
use App\Http\Request\Pages\StorePageRequest;
use App\Http\Requests\Pages\UpdatePageRequest;

class PagesController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $this->permission('pages:browse');

        return PageResource::collection(Page::query()
            ->orderBy('created_at', 'desc')
            ->paginate(50));
    }

    /**
     * @param StorePageRequest $request
     * @return PageResource
     */
    public function store(StorePageRequest $request)
    {
        $this->permission('pages:create');

        $page = new Page;
        $page->slug = Slug::normalize($request->slug);
        $page->title = $request->title;
        $page->content = $request->content;
        $page->seo_title = $request->seo_title;
        $page->seo_description = $request->seo_description;
        $page->seo_keywords = $request->seo_keywords;
        $page->is_system = false;
        $page->is_contentable = true;
        $page->robots_noindex = $request->robots_noindex;
        $page->robots_nofollow = $request->robots_nofollow;
        $page->save();

        return new PageResource($page);
    }

    /**
     * @param Page $page
     * @return PageResource
     */
    public function show(Page $page)
    {
        $this->permission('pages:browse');

        return new PageResource($page);
    }

    /**
     * @param UpdatePageRequest $request
     * @param Page $page
     * @return PageResource
     */
    public function update(UpdatePageRequest $request, Page $page)
    {
        $this->permission('pages:update');

        $page->title = $request->title;
        if ($page->is_contentable) {
            $page->content = $request->content;
        }
        $page->seo_title = $request->seo_title;
        $page->seo_description = $request->seo_description;
        $page->seo_keywords = $request->seo_keywords;
        $page->robots_noindex = $request->robots_noindex;
        $page->robots_nofollow = $request->robots_nofollow;
        // У системных страниц нельзя менять идентификатор
        if (!$page->is_system) {
            $page->slug = Slug::normalize($request->slug);
        }
        $page->save();

        return new PageResource($page);
    }

    /**
     * @param Page $page
     * @throws \Exception
     */
    public function destroy(Page $page)
    {
        $this->permission('pages:delete');

        // Нельзя удалять системные страницы
        if ($page->is_system) {
            abort(403);
        }

        $page->delete();
    }
}
