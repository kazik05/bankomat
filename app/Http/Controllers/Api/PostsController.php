<?php

namespace App\Http\Controllers\Api;

use App\Post;
use App\Slug;
use App\Http\Resources\PostResource;
use App\Http\Controllers\Controller;
use App\Http\Request\Posts\StorePostRequest;
use App\Http\Requests\Posts\UpdatePostRequest;

class PostsController extends Controller
{
    /**
     *
     */
    public function index()
    {
        $this->permission('posts:browse');

        return PostResource::collection(Post::query()
            ->orderBy('created_at', 'desc')
            ->paginate(50));
    }

    /**
     * @param StorePostRequest $request
     * @return PostResource
     */
    public function store(StorePostRequest $request)
    {
        $this->permission('posts:create');

        $post = new Post;
        $post->slug = Slug::normalize($request->slug);
        $post->title = $request->title;
        $post->image = $request->image;
        $post->description = $request->description;
        $post->content = $request->content;
        $post->seo_title = $request->seo_title;
        $post->seo_description = $request->seo_description;
        $post->seo_keywords = $request->seo_keywords;
        $post->robots_noindex = $request->robots_noindex;
        $post->robots_nofollow = $request->robots_nofollow;
        $post->save();

        return new PostResource($post);
    }

    /**
     * @param Post $post
     * @return PostResource
     */
    public function show(Post $post)
    {
        $this->permission('posts:browse');

        return new PostResource($post);
    }

    /**
     * @param UpdatePostRequest $request
     * @param Post $post
     * @return PostResource
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        $this->permission('posts:update');

        $post->title = $request->title;
        $post->image = $request->image;
        $post->slug = Slug::normalize($request->slug);
        $post->description = $request->description;
        $post->content = $request->content;
        $post->seo_title = $request->seo_title;
        $post->seo_description = $request->seo_description;
        $post->seo_keywords = $request->seo_keywords;
        $post->robots_noindex = $request->robots_noindex;
        $post->robots_nofollow = $request->robots_nofollow;
        $post->save();

        return new PostResource($post);
    }

    /**
     * @param Post $post
     * @throws \Exception
     */
    public function destroy(Post $post)
    {
        $this->permission('posts:delete');

        $post->delete();
    }
}
