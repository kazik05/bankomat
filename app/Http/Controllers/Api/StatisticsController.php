<?php

namespace App\Http\Controllers\Api;

use App\Click;
use App\Country;
use App\Partner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\StatisticsResource;

class StatisticsController extends Controller
{
    /**
     * @param Request $request
     * @return StatisticsResource
     */
    public function clicks(Request $request)
    {
        $this->permission('statistics:browse');

        $request->validate([
            'partner' => 'nullable|integer|exists:partners,id',
            'country' => 'nullable|integer|exists:countries,id',
            'browser' => 'nullable|string',
            'dates' => 'nullable|string|regex:/^\d{2}\.\d{2}\.\d{4}\s+-\s+\d{2}\.\d{2}\.\d{4}$/',
        ]);

        return new StatisticsResource($this->query($request)->orderBy('created_at', 'desc')->paginate(50));
    }

    public function query($request)
    {
        $query = Click::query();

        if ($request->has('partner')) {
            $query->where('partner_id', $request->partner);
        }

        if ($request->has('country')) {
            $query->where('country_id', $request->country);
        }

        if ($request->has('browser')) {
            $query->where('client_name', $request->browser);
        }

        if ($request->has('dates') && !empty($request->dates)) {
            list($start, $finish) = explode(' - ', $request->dates);
            $start = Carbon::createFromFormat('d.m.Y', $start);
            $finish = Carbon::createFromFormat('d.m.Y', $finish);
            $query->where('created_at', '>', $start);
            $query->where('created_at', '<', $finish);
        }

        return $query;
    }

    public function filters()
    {
        $this->permission('statistics:browse');

        return response()->json([
            'partners' => Partner::query()->get(['id', 'name']),
            'countries' => Country::query()->get(['id', 'name']),
            'browsers' => Click::query()
                ->groupBy('client_name')
                ->get(['client_name'])
                ->pluck('client_name')
                ->toArray(),
        ]);
    }
}
