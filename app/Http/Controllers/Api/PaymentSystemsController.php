<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentSystems\StorePaymentSystemRequest;
use App\Http\Requests\PaymentSystems\UpdatePaymentSystemRequest;
use App\Http\Resources\PaymentSystemResource;
use App\PaymentSystem;
use Illuminate\Http\Request;

class PaymentSystemsController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $this->permission('payment_systems:browse');

        return PaymentSystemResource::collection(PaymentSystem::query()
            ->orderBy('created_at', 'desc')
            ->paginate(50));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function select()
    {
        $this->permission('payment_systems:browse');

        return response()->json([
            'data' => PaymentSystem::get(['id', 'name'])->toArray(),
        ]);
    }

    /**
     * @param StorePaymentSystemRequest $request
     * @return PaymentSystemResource
     */
    public function store(StorePaymentSystemRequest $request)
    {
        $this->permission('payment_systems:create');

        $paymentSystem = new PaymentSystem;
        $paymentSystem->slug = $request->slug;
        $paymentSystem->name = $request->name;
        $paymentSystem->save();

        return new PaymentSystemResource($paymentSystem);
    }

    /**
     * @param PaymentSystem $paymentSystem
     * @return PaymentSystemResource
     */
    public function show(PaymentSystem $paymentSystem)
    {
        $this->permission('payment_systems:browse');

        return new PaymentSystemResource($paymentSystem);
    }

    /**
     * @param UpdatePaymentSystemRequest $request
     * @param PaymentSystem $paymentSystem
     * @return PaymentSystemResource
     */
    public function update(UpdatePaymentSystemRequest $request, PaymentSystem $paymentSystem)
    {
        $this->permission('payment_systems:update');

        $paymentSystem->slug = $request->slug;
        $paymentSystem->name = $request->name;
        $paymentSystem->save();

        return new PaymentSystemResource($paymentSystem);
    }

    /**
     * @param PaymentSystem $paymentSystem
     * @throws \Exception
     */
    public function destroy(PaymentSystem $paymentSystem)
    {
        $this->permission('payment_systems:delete');

        $paymentSystem->delete();
    }
}
