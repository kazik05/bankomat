<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Resources\AuthResource;
use App\Http\Resources\UserResource;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    /**
     * @param Request $request
     * @return AuthResource|\Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (!$token = JWTAuth::attempt($credentials)) {
            return $this->error(401, 'Invalid credentials');
        }

        return new AuthResource(JWTAuth::user(), $token);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Tymon\JWTAuth\Exceptions\JWTException
     */
    public function refresh()
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return $this->success([
                'token' => JWTAuth::refresh(),
                'expires_in' => config('jwt.ttl') * 60 + time(),
            ]);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Tymon\JWTAuth\Exceptions\JWTException
     */
    public function logout()
    {
        JWTAuth::parseToken()->authenticate();
        JWTAuth::invalidate();

        return $this->success('Successfully logged out');
    }

    /**
     * @return UserResource
     * @throws \Tymon\JWTAuth\Exceptions\JWTException
     */
    public function user()
    {
        return new UserResource(JWTAuth::user());
    }
}
