<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Roles\StoreRoleRequest;
use App\Http\Requests\Roles\UpdateRoleRequest;
use App\Http\Resources\RoleResource;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class RolesController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $this->permission('roles:browse');

        return RoleResource::collection(Role::query()
            ->orderBy('created_at', 'desc')
            ->paginate(50));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function select()
    {
        $this->permission('roles:browse');

        return response()->json([
            'data' => Role::get(['id', 'name', 'level'])
                ->toArray(),
        ]);
    }

    /**
     * @param StoreRoleRequest $request
     * @return RoleResource
     */
    public function store(StoreRoleRequest $request)
    {
        $this->permission('roles:create');

        $role = new Role;
        $role->slug = $request->slug;
        $role->name = $request->name;
        // Можно установить уровень роли только меньше чем у пользователя
        if (JWTAuth::user()->level > $request->level) {
            $role->level = $request->level;
        }
        $role->save();

        // Можно устанавливать права только которые есть у пользователя
        if (is_array($request->permissions)) {
            $userPermissions = [];
            foreach(JWTAuth::user()->roles as $userRole) {
                $userPermissions = array_merge($userPermissions, $userRole->permissions()->get(['permissions.id'])->pluck('id')->toArray());
            }
            $pivot = [];
            foreach($request->permissions as $permissionId) {
                if (in_array($permissionId, $userPermissions)) {
                    $pivot[] = [
                        'permission_id' => $permissionId,
                        'role_id' => $role->id,
                    ];
                }
            }
            DB::table('role_permissions')->insert($pivot);
        }

        return new RoleResource($role);
    }

    /**
     * @param Role $role
     * @return RoleResource
     */
    public function show(Role $role)
    {
        $this->permission('roles:browse');
        return new RoleResource($role);
    }

    /**
     * @param UpdateRoleRequest $request
     * @param Role $role
     * @return RoleResource
     */
    public function update(UpdateRoleRequest $request, Role $role)
    {
        $this->permission('roles:update');
        if (JWTAuth::user()->level < $role->level) {
            abort(403);
        }
        $role->slug = $request->slug;
        $role->name = $request->name;
        // Можно установить уровень роли только меньше чем у пользователя
        if (JWTAuth::user()->level > $request->level) {
            $role->level = $request->level;
        }
        $role->save();

        DB::table('role_permissions')->where('role_id', $role->id)->delete();

        // Можно устанавливать права только которые есть у пользователя
        if (is_array($request->permissions)) {
            $userPermissions = [];
            foreach(JWTAuth::user()->roles as $userRole) {
                $userPermissions = array_merge($userPermissions, $userRole->permissions()->get(['permissions.id'])->pluck('id')->toArray());
            }
            $pivot = [];
            foreach($request->permissions as $permissionId) {
                if (in_array($permissionId, $userPermissions)) {
                    $pivot[] = [
                        'permission_id' => $permissionId,
                        'role_id' => $role->id,
                    ];
                }
            }
            DB::table('role_permissions')->insert($pivot);
        }

        return new RoleResource($role);
    }

    /**
     * @param Role $role
     * @throws \Exception
     */
    public function destroy(Role $role)
    {
        $this->permission('roles:delete');
        if (JWTAuth::user()->level < $role->level) {
            abort(403);
        }
        $role->delete();
    }
}
