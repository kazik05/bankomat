<?php

namespace App\Http\Controllers\Api;

use App\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PartnerRequest;
use App\Http\Resources\PartnerResource;
use Illuminate\Support\Facades\DB;

class PartnersController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $this->permission('partners:browse');

        $query = Partner::query();

        if ($request->has('type')) {
            $query->where('type', $request->type);
        }

        return PartnerResource::collection($query
            ->orderBy('created_at', 'desc')
            ->paginate(50));
    }

    /**
     * @param PartnerRequest $request
     * @return PartnerResource
     */
    public function store(PartnerRequest $request)
    {
        $this->permission('partners:create');

        $partner = new Partner;
        $partner->name = $request->name;
        $partner->image = $request->image;
        $partner->rate = $request->rate;
        $partner->min = $request->min;
        $partner->max = $request->max;
        $partner->min_age = $request->min_age;
        $partner->max_age = $request->max_age;
        $partner->min_term = $request->min_term;
        $partner->max_term = $request->max_term;
        $partner->link = $request->link;
        $partner->type = $request->type;
        $partner->grace_period = $request->grace_period;
        $partner->is_active = $request->is_active;
        $partner->external_id = $request->external_id;
        $partner->save();

        if (is_array($request->payment_systems)) {
            DB::table('partner_payment_systems')->insert(array_map(function($paymentSystemId) use($partner) {
                return [
                    'partner_id' => $partner->id,
                    'payment_system_id' => $paymentSystemId,
                ];
            }, $request->payment_systems));
        }

        return new PartnerResource($partner);
    }

    /**
     * @param Partner $partner
     * @return PartnerResource
     */
    public function show(Partner $partner)
    {
        $this->permission('partners:browse');

        return new PartnerResource($partner);
    }

    /**
     * @param PartnerRequest $request
     * @param Partner $partner
     * @return PartnerResource
     */
    public function update(PartnerRequest $request, Partner $partner)
    {
        $this->permission('partners:update');

        $partner->name = $request->name;
        $partner->image = $request->image;
        $partner->rate = $request->rate;
        $partner->min = $request->min;
        $partner->max = $request->max;
        $partner->min_age = $request->min_age;
        $partner->max_age = $request->max_age;
        $partner->min_term = $request->min_term;
        $partner->max_term = $request->max_term;
        $partner->link = $request->link;
        $partner->type = $request->type;
        $partner->grace_period = $request->grace_period;
        $partner->is_active = $request->is_active;
        $partner->external_id = $request->external_id;
        $partner->save();

        DB::table('partner_payment_systems')->where('partner_id', $partner->id)->delete();

        if (is_array($request->payment_systems)) {
            DB::table('partner_payment_systems')->insert(array_map(function($paymentSystemId) use($partner) {
                return [
                    'partner_id' => $partner->id,
                    'payment_system_id' => $paymentSystemId,
                ];
            }, $request->payment_systems));
        }

        return new PartnerResource($partner);
    }

    /**
     * @param Partner $partner
     * @throws \Exception
     */
    public function destroy(Partner $partner)
    {
        $this->permission('partners:delete');

        $partner->delete();
    }
}
