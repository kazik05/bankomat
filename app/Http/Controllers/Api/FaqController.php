<?php

namespace App\Http\Controllers\Api;

use App\Faq;
use App\Http\Controllers\Controller;
use App\Http\Requests\FaqRequest;
use App\Http\Requests\LangRequest;
use App\Http\Resources\FaqResource;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $this->permission('faq:browse');

        return FaqResource::collection(Faq::query()
            ->orderBy('created_at', 'desc')
            ->paginate(50));
    }

    /**
     * @param FaqRequest $request
     * @return FaqResource
     */
    public function store(FaqRequest $request)
    {
        $this->permission('faq:create');

        $faq = new Faq;
        $faq->answer = $request->answer;
        $faq->question = $request->question;
        $faq->icon = $request->icon;
        $faq->save();

        return new FaqResource($faq);
    }

    /**
     * @param Faq $faq
     * @return FaqResource
     */
    public function show(Faq $faq)
    {
        $this->permission('faq:browse');
        return new FaqResource($faq);
    }

    /**
     * @param FaqRequest $request
     * @param Faq $faq
     * @return FaqResource
     */
    public function update(FaqRequest $request, Faq $faq)
    {
        $this->permission('faq:update');

        $faq->answer = $request->answer;
        $faq->question = $request->question;
        $faq->icon = $request->icon;
        $faq->save();

        return new FaqResource($faq);
    }

    /**
     * @param Faq $faq
     * @throws \Exception
     */
    public function destroy(Faq $faq)
    {
        $this->permission('faq:delete');

        $faq->delete();
    }
}
