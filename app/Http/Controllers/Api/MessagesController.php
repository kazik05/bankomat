<?php

namespace App\Http\Controllers\Api;

use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\MessageResource;
use App\Http\Requests\Messages\StoreMessageRequest;
use App\Http\Requests\Messages\UpdateMessageRequest;

class MessagesController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $this->permission('messages:browse');

        return MessageResource::collection(Message::query()
            ->orderBy('created_at', 'desc')
            ->paginate(50));
    }

    /**
     * @param StoreMessageRequest $request
     * @return MessageResource
     */
    public function store(StoreMessageRequest $request)
    {
        $this->permission('messages:create');

        $message = new Message;
        $message->key = $request->key;
        $message->message = $request->message;
        $message->save();

        return new MessageResource($message);
    }

    /**
     * @param Message $message
     * @return MessageResource
     */
    public function show(Message $message)
    {
        $this->permission('messages:browse');

        return new MessageResource($message);
    }

    /**
     * @param UpdateMessageRequest $request
     * @param Message $message
     * @return MessageResource
     */
    public function update(UpdateMessageRequest $request, Message $message)
    {
        $this->permission('messages:update');

        if (!$message->is_system) {
            $message->key = $request->key;
        }
        $message->message = $request->message;
        $message->save();

        return new MessageResource($message);
    }

    /**
     * @param Message $message
     * @throws \Exception
     */
    public function destroy(Message $message)
    {
        $this->permission('messages:delete');

        if ($message->is_system) {
            abort(403);
        }

        $message->delete();
    }
}
