<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingRequest;
use App\Http\Resources\SettingsResource;
use App\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function get(SettingRequest $request)
    {
        return SettingsResource::collection(Setting::query()->whereIn('key', $request->settings)->get());
    }

    public function set(SettingRequest $request)
    {
        foreach($request->settings as $key => $value) {
            switch($key) {
                case 'head_append':
                case 'body_append':
                    $this->permission('settings:scripts');
                    break;

                case 'social_telegram':
                case 'social_vk':
                case 'social_facebook':
                    $this->permission('settings:social');
                    break;

                case 'leads_su_token':
                case 'admitad_com_id':
                case 'admitad_com_secret':
                    $this->permission('settings:api');
                    break;

                default:
                    abort(403);
                    break;
            }

            Setting::set($key, $value);
        }
    }
}
