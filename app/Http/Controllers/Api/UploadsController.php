<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\CropRequest;
use App\Http\Controllers\Controller;

class UploadsController extends Controller
{
    /**
     * @param CropRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function crop(CropRequest $request)
    {
        $img = \Image::make($request->file('image'));
        $img->crop((int) $request->width, (int) $request->height, (int) $request->x, (int) $request->y);
        switch($request->type) {
            case 'avatar':
                $img->resize(100, 100);
                break;

            case 'post':
                $img->resize(800, 600);
                break;

            case 'partner':
                $img->resize(143, 59);
                break;

            case 'card':
                $img->resize(300, 190);
                break;
        }

        $ext = $request->file('image')->getClientOriginalExtension();

        $name = $this->getAvailableFileName($ext);

        $path = public_path($name);
        $url = url($name);

        $img->save($path, 100, $ext);

        return response()->json([
            'url' => $url,
        ]);
    }

    /**
     * @param string $ext
     * @return string
     */
    protected function getAvailableFileName($ext)
    {
        $directory = 'storage/' . date('F') . date('Y');
        $publicDirectoryPath = public_path($directory);
        if (!is_dir($publicDirectoryPath)) {
            mkdir($publicDirectoryPath, 0755);
            chmod($publicDirectoryPath, 0755);
        }
        $name = $directory . '/' . uniqid(time()) . '.' . $ext;
        while(file_exists(public_path($name))) {
            $name = $directory . '/' . uniqid(time()) . '.' . $ext;
        }

        return $name;
    }
}
