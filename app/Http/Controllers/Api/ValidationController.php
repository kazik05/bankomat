<?php

namespace App\Http\Controllers\Api;

use App\Message;
use Illuminate\Http\Request;
use App\Rules\SlugUniqueRule;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ValidationController extends Controller
{
    public function isUniqueSlug(Request $request)
    {
        $this->validate($request, [
            'table' => 'required|string|in:pages,posts',
            'column' => 'required|string|in:slug',
            'ignore' => 'nullable|integer',
            'value' => 'nullable|string',
        ]);

        return $this->result((new SlugUniqueRule($request->table, $request->column, $request->ignore))->passes('__NONE__', $request->value));
    }

    public function isUnique(Request $request)
    {
        $this->validate($request, [
            'table' => 'required|string|in:messages,payment_systems,roles,users',
            'column' => 'required|string|in:key,slug,email',
            'ignore' => 'nullable|integer',
            'value' => 'nullable|string',
        ]);

        return $this->result(DB::table($request->table)
                ->where($request->column, $request->value)
                ->when($request->has('ignore'), function($query) use($request) {
                    return $query->where('id', '!=', $request->ignore);
                })
                ->count() == 0);
    }

    protected function result($result)
    {
        return response()->json([
            'result' => $result,
        ]);
    }
}
