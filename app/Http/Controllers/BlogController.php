<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function post($slug)
    {
        $post = Post::query()
            ->where('slug', '/' . $slug)
            ->firstOrFail();

        return view('front.blog.post', [
            'post' => $post,
            'similar' => Post::query()
                ->where('id', '!=', $post->id)
                ->inRandomOrder()
                ->limit(3)
                ->get(),
        ]);
    }
}
