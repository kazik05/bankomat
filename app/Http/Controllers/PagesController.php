<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     * @return mixed
     * @throws \ReflectionException
     */
    public function home()
    {
        $page = Page::query()
            ->where('slug',  '/')
            ->firstOrFail();

        return $this->render($page);
    }

    /**
     * @param string $slug
     * @return mixed
     * @throws \ReflectionException
     */
    public function show($slug)
    {
        $page = Page::query()
            ->where('slug', '/' . $slug)
            ->firstOrFail();

        return $this->render($page);
    }

    /**
     * @param Page $page
     * @return mixed
     * @throws \ReflectionException
     */
    protected function render(Page $page)
    {
        $reflectionClass = new \ReflectionClass($page->renderer);
        $rendererInstance = $reflectionClass->newInstanceArgs([$page]);

        return $rendererInstance->render();
    }
}
