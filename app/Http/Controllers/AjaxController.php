<?php

namespace App\Http\Controllers;

use App\Partner;
use App\PaymentSystem;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\PartnerResource;

class AjaxController extends Controller
{
    public function partners(Request $request)
    {
        $request->validate([
            'type' => ['required', 'string', Rule::in([Partner::TYPE_LOAN, Partner::TYPE_CARD, Partner::TYPE_CASH, Partner::TYPE_REFINANCE])],
        ]);

        return PartnerResource::collection(Partner::query()
            ->where('link', '!=', null)
            ->where('is_active', true)
            ->where('type', $request->type)
            ->orderBy('clicks_counter', 'desc')
            ->get());
    }

    public function paymentSystems()
    {
        return response()->json([
            'data' => PaymentSystem::query()->get([
                'id',
                'name',
                'slug',
            ]),
        ]);
    }
}
