<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'integer',
        'geoname_id' => 'integer',
        'country_id' => 'integer',
    ];
    public $timestamps = false;

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
