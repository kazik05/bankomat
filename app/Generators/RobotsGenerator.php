<?php

namespace App\Generators;

use App\Page;
use App\Post;

class RobotsGenerator extends AbstractGenerator
{
    protected static $lines = [];

    protected static function append($line)
    {
        static::$lines[] = $line;
    }

    public static function name()
    {
        return 'robots.txt';
    }

    protected static function write()
    {
        @file_put_contents(static::path(), implode("\n", static::$lines));
        static::$lines = [];
    }

    public static function getDisallowPages()
    {
        return Page::query()
            ->where('robots_noindex', true)
            ->where('robots_nofollow', true)
            ->get();
    }

    public static function getDisallowPosts()
    {
        return Post::query()
            ->where('robots_noindex', true)
            ->where('robots_nofollow', true)
            ->get();
    }

    public static function userAgent($userAgent)
    {
        static::append('User-agent: ' . $userAgent);
    }

    public static function disallow($url)
    {
        static::append('Disallow: ' . str_replace(url('/'), '', $url));
    }

    public static function sitemap($url)
    {
        static::append('Sitemap: ' . $url);
    }

    public static function generate()
    {
        static::userAgent('*');
        static::disallow('/admin');
        static::disallow('/assets');
        static::disallow('/ajax');
        static::disallow('/c');

        foreach(static::getDisallowPages() as $page) {
            static::disallow($page->url);
        }

        foreach(static::getDisallowPosts() as $post) {
            static::disallow($post->url);
        }

        if (SitemapGenerator::exists()) {
            static::sitemap(SitemapGenerator::url());
        }

        static::write();
    }
}
