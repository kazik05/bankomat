<?php

namespace App\Generators;

use App\Page;
use App\Post;

class SitemapGenerator extends AbstractGenerator
{
    protected static $sitemap = '';

    protected static function headerTag()
    {
        static::$sitemap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    }

    protected static function urlsetTag($callback)
    {
        static::$sitemap .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
        $callback();
        static::$sitemap .= '</urlset>';
    }

    protected static function urlTag($url, $priority = 0.8, $lastmod = null, $changefreq = 'monthly')
    {
        static::$sitemap .= "    <url>\n";
        static::$sitemap .= "        <loc>$url</loc>\n";
        if ($lastmod !== null) {
            static::$sitemap .= "        <lastmod>" . $lastmod->format('c') . "</lastmod>\n";
        }
        static::$sitemap .= "        <priority>$priority</priority>\n";
        static::$sitemap .= "        <changefreq>$changefreq</changefreq>\n";
        static::$sitemap .= "    </url>\n";
    }

    protected static function write()
    {
        @file_put_contents(static::path(), static::$sitemap);
        static::$sitemap = '';
    }

    public static function name()
    {
        return 'sitemap.xml';
    }

    public static function generate()
    {
        static::headerTag();
        static::urlsetTag(function() {
            foreach(Page::all() as $page) {
                if ($page->url) {
                    static::urlTag($page->url, $page->slug == '/' ? 1.0 : 0.9, $page->updated_at);
                }
            }
            foreach(Post::all() as $post) {
                if ($post->url) {
                    static::urlTag($post->url, 0.7, $post->updated_at);
                }
            }
        });
        static::write();
    }
}

