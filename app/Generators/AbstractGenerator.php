<?php

namespace App\Generators;

abstract class AbstractGenerator
{
    public static function path()
    {
        return public_path(static::name());
    }

    public static function exists()
    {
        return file_exists(static::path());
    }

    public static function url()
    {
        return url(static::name());
    }

    abstract public static function name();
    abstract public static function generate();
}
