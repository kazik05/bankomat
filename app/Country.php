<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'integer',
        'geoname_id' => 'integer',
    ];
    public $timestamps = false;

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
