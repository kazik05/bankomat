<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', 'AdminController@index');
Route::get('/admin/{slug}', 'AdminController@index')->where('slug', '.+');

Route::post('/ajax/partners', 'AjaxController@partners');
Route::post('/ajax/payment-systems', 'AjaxController@paymentSystems');

Route::get('/', 'PagesController@home')->middleware('html-cache');
Route::get('/c/{hash}', 'StatisticsController@click');
Route::get('/blog/{slug}', 'BlogController@post')->middleware('html-cache');
Route::get('/{slug}', 'PagesController@show')->middleware('html-cache');
