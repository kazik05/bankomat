<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function() {
    Route::post('login', 'LoginController@login')->name('login');
    Route::post('refresh', 'LoginController@refresh')->name('refresh');

    Route::middleware('auth:api')->group(function() {
        Route::get('user', 'LoginController@user')->name('user');
        Route::post('logout', 'LoginController@logout')->name('logout');
    });
});

Route::post('validate/is-unique-slug', 'ValidationController@isUniqueSlug')->name('validate.is-unique-slug');
Route::post('validate/is-unique', 'ValidationController@isUnique')->name('validate.is-unique');

Route::apiResource('pages', 'PagesController');
Route::apiResource('posts', 'PostsController');
Route::apiResource('faq', 'FaqController');
Route::apiResource('reviews', 'ReviewsController');
Route::apiResource('partners', 'PartnersController');
Route::apiResource('messages', 'MessagesController');
Route::get('payment-systems/select', 'PaymentSystemsController@select');
Route::apiResource('payment-systems', 'PaymentSystemsController');
Route::apiResource('permissions', 'PermissionsController')->only(['index']);
Route::get('roles/select', 'RolesController@select');
Route::apiResource('roles', 'RolesController');
Route::apiResource('users', 'UsersController');

Route::get('statistics/clicks', 'StatisticsController@clicks');
Route::get('statistics/filters', 'StatisticsController@filters');

Route::post('uploads/crop', 'UploadsController@crop');

Route::post('settings/get', 'SettingsController@get');
Route::post('settings/set', 'SettingsController@set');

