<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClicksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clicks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('partner_id');
            $table->string('ip')->nullable();
            $table->bigInteger('country_id')->unsigned()->nullable();
            $table->foreign('country_id')->on('countries')->references('id')->onDelete(DB::raw('SET NULL'));
            $table->bigInteger('city_id')->unsigned()->nullable();
            $table->foreign('city_id')->on('cities')->references('id')->onDelete(DB::raw('SET NULL'));
            $table->text('user_agent')->nullable();
            $table->text('referer')->nullable();
            $table->string('referer_domain')->nullable();
            $table->boolean('is_bot')->default(false);
            $table->string('bot')->nullable();
            $table->string('os')->nullable();
            $table->string('client_type')->nullable();
            $table->string('client_name')->nullable();
            $table->string('client_version')->nullable();
            $table->string('device_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clicks');
    }
}
