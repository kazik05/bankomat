<?php

use App\Partner;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', [Partner::TYPE_CARD, Partner::TYPE_CASH, Partner::TYPE_LOAN, Partner::TYPE_REFINANCE]);
            $table->string('name');
            $table->string('image');
            $table->double('rate');
            $table->integer('min')->unsigned();
            $table->integer('max')->unsigned();
            $table->integer('min_age')->unsigned()->nullable();
            $table->integer('max_age')->unsigned()->nullable();
            $table->integer('min_term')->unsigned()->nullable();
            $table->integer('max_term')->unsigned()->nullable();
            $table->string('link')->nullable();
            $table->integer('grace_period')->unsigned()->nullable();
            $table->string('hash')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
