<?php

use App\Message;
use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->message('navbar:home', 'Главная');
        $this->message('navbar:loan', 'Микрозаймы');
        $this->message('navbar:reviews', 'Отзывы');
        $this->message('navbar:faq', 'FAQ');
        $this->message('navbar:blog', 'Блог');
        $this->message('navbar:cash', 'Кредит наличными');
        $this->message('navbar:credit-card', 'Кредитные карты');

        $this->message('quote:text', 'Если на деловое предложение банкир отвечает «да», это значит «может быть»; если он отвечает «может быть», это значит «да»; а если он не задумываясь отвечает «нет», значит, это плохой банкир.');
        $this->message('quote:author', 'Андре Костолани');

        $this->message('call-to-action:title', 'Нужны деньги?');
        $this->message('call-to-action:text', 'Наш сайт поможет выбрыть вам выбрать подходящую организацию для оформления займа.');

        $this->message('reviews:title', 'Что говорят пользователи о нас?');
        $this->message('reviews:description', 'Сотни тысяч человек используют банковские услуги каждый день. Наш сайт помогает найти лучшее предложение среди великого множества. Несколько отзывов от наших постоянных пользователей.');

        $this->message('blog:title', 'Блог');
        $this->message('blog:view-all', 'Показать все');
        $this->message('blog:similar-title', 'Читайте также');
        $this->message('blog:empty', 'Пока нет ни одной записи, подождите немного скоро что то появится.');

        $this->message('faq:title', 'Ваши вопросы');
        $this->message('faq:description', 'Здесь мы собрали самые частые вопросы о нашем сервисе');
        $this->message('faq:view-all', 'Смотреть все');

        $this->message('footer:brand', preg_replace('/^https?:\/\//', '', url('/')));
        $this->message('footer:copyrights', 'Все права защищены!');
        $this->message('footer:description', 'Наша организация поможет вым подобрать лучшее предложение на рынке в сфере кредитования!');
        $this->message('footer:about-title', 'О нас');
        $this->message('footer:blog', 'Блог');
        $this->message('footer:about', 'О нас');
        $this->message('footer:info-title', 'Информация');
        $this->message('footer:loan', 'Микрозаймы');
        $this->message('footer:courses', 'Курсы валют');
        $this->message('footer:online', 'Займ на эл. кошелёк');
        $this->message('footer:card', 'Займ на карту');
        $this->message('footer:cash', 'Кредит наличными');
        $this->message('footer:credit-card', 'Кредитные карты');
        $this->message('footer:faq', 'FAQ');

        $this->message('loan:title', 'Микразаймы');
        $this->message('loan:description', 'Нужен микрозайм? Мы поможем подобрать организацию.');

        $this->message('cash:title', 'Кредит наличными');
        $this->message('cash:description', 'Нужна большая сумма на отдых или приобретение автомобиля?');

        $this->message('online:title', 'Займ на эл. кошелёк');
        $this->message('online:description', 'Получите микрозайм не выходя из дома, не вставая с дивана.');

        $this->message('card:title', 'Займ на карту');
        $this->message('card:description', 'Нужен займ? Подберите организацию для получения займа на вашу карту.');

        $this->message('credit-card:title', 'Кредитные карты');
        $this->message('credit-card:description', 'У вас всегда будет хороший друг который не против занять тысячу до зарплаты.');

        $this->message('404-page:title', 'Страница не найдена');
        $this->message('404-page:return-home', 'Вернуться домой?');
    }

    protected function message($key, $message)
    {
        $model = Message::where('key', $key)->first();
        if (!$model) {
            $model = new Message;
            $model->key = $key;
            $model->message = $message;
            $model->is_system = true;
            $model->save();
        }
    }
}
