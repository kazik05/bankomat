<?php

use App\Review;
use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->review('Лидия Киселёва', url('assets/images/avatars/avatar1.jpg'), 'Икала где взять денег на покупку подарка мужу на день рождения. Сервис помог подобрать МФО. Спасибо!');
        $this->review('Михаил Никифоров', url('assets/images/avatars/avatar2.jpg'), 'Крутой сервис! Очень доволен!');
        $this->review('Анна Никифорова', url('assets/images/avatars/avatar3.jpg'), 'Сайт помог найти МФО с ставкой по кредиту в 0%, сама не ожидала что так получится. Наверное лучше предложения быть не может.');
        $this->review('Левани Капанадзе', url('assets/images/avatars/avatar4.jpg'), 'Сервис сделан грамотно, все проработано очень удобно пользоваться.');
    }

    protected function review($name, $avatar, $text)
    {
        $review = new Review;
        $review->name = $name;
        $review->image = $avatar;
        $review->review = $text;
        $review->save();
    }
}
