<?php

use App\Role;
use App\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role;
        $role->slug = 'admin';
        $role->name = 'Администратор';
        $role->level = 1000000;
        $role->save();

        $pivot = [];
        foreach(Permission::all() as $permission) {
            $pivot[] = [
                'permission_id' => $permission->id,
                'role_id' => $role->id,
            ];
        }
        DB::table('role_permissions')->insert($pivot);
    }
}
