<?php

use App\City;
use App\Click;
use App\Country;
use App\Partner;
use GeoIp2\Database\Reader;
use Illuminate\Database\Seeder;
use DeviceDetector\DeviceDetector;

class DummyClicksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        foreach(Partner::all() as $partner) {
            $len = rand(10, 100);
            for($i = 0; $i < $len; $i++) {
                $click = new Click;
                $click->partner_id = $partner->id;
                $date = $faker->dateTime;
                $click->created_at = $date;
                $click->updated_at = $date;

                $ip = $faker->ipv4;
                if (!empty($ip)) {
                    $click->ip = $ip;
                    try {
                        $reader = new Reader(base_path('vendor/lysenkobv/maxmind-geolite2-database/city.mmdb'));
                        $record = $reader->city($ip);
                        if ($record->country->geonameId !== null) {
                            $country = Country::query()
                                ->where('geoname_id', $record->country->geonameId)
                                ->first();
                            if (!$country) {
                                $country = new Country;
                                $country->geoname_id = $record->country->geonameId;
                                $country->code = $record->country->isoCode;
                                $country->name = $record->country->names['ru'];
                                $country->save();
                            }
                            $click->country_id = $country->id;
                            if ($record->city->geonameId !== null) {
                                $city = City::query()
                                    ->where('geoname_id', $record->city->geonameId)
                                    ->first();
                                if (!$city) {
                                    $city = new City;
                                    $city->geoname_id = $record->city->geonameId;
                                    $city->name = $record->city->names['ru'];
                                    $city->country_id = $country->id;
                                    $city->save();
                                }
                                $click->city_id = $city->id;
                            }
                        }
                    } catch (\Exception $e) {
                    }
                }

                $referer = $faker->url;
                if (!empty($referer)) {
                    $click->referer = $referer;
                    $click->referer_domain = parse_url($referer, PHP_URL_HOST);
                }

                $ua = $faker->userAgent;
                if (!empty($ua)) {
                    $click->user_agent = $ua;
                    $dd = new DeviceDetector($ua);
                    $dd->parse();
                    if ($dd->isBot()) {
                        $click->is_bot = true;
                        $click->bot = $dd->getBot();
                    } else {
                        $click->os = $dd->getOs()['name'] ?? null;
                        $client = $dd->getClient();
                        $click->client_type = $client['type'];
                        $click->client_name = $client['name'];
                        $click->client_version = $client['version'];
                        $click->device_type = $dd->getDeviceName();
                    }
                }

                $click->save();
            }
        }
    }
}
