<?php

use App\PaymentSystem;
use Illuminate\Database\Seeder;

class PaymentSystemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->system('bank_account', 'Банковский счет');
        $this->system('visa', 'VISA');
        $this->system('mir', 'МИР');
        $this->system('maestro', 'Maestro');
        $this->system('master_card', 'Master Card');
        $this->system('qiwi', 'QIWI');
        $this->system('yandex_money', 'Яндекс.Деньги');
        $this->system('contact', 'CONTACT');
        $this->system('koronapay', 'Золотая Корона');
        $this->system('cash', 'Наличные в кассе');
        $this->system('unistream', 'Юнистрим');
    }

    protected function system($slug, $name)
    {
        $system = PaymentSystem::where('slug', $slug)->first();
        if (!$system) {
            $system = new PaymentSystem;
            $system->slug = $slug;
        }
        $system->name = $name;
        $system->save();
    }
}
