<?php

use App\User;
use App\Role;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.admin',
            'password' => bcrypt('admin@admin.admin'),
        ]);
        $role = Role::query()->where('slug', 'admin')->firstOrFail();

        DB::table('user_roles')->insert([
            'role_id' => $role->id,
            'user_id' => $user->id,
        ]);
    }
}
