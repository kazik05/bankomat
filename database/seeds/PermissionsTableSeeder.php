<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    protected $group = null;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->group('Права', function() {
            $this->permission('permissions:browse', 'Просмотр прав');
        });
        $this->group('Статистика', function() {
            $this->permission('statistics:browse', 'Просмотр статистики');
        });
        $this->generate('Пользователи', 'users', 'пользователей');
        $this->generate('Роли', 'roles', 'ролей');
        $this->generate('FAQ', 'faq', 'FAQ');
        $this->generate('Строки', 'messages', 'сообщений');
        $this->generate('Страницы', 'pages', 'страниц');
        $this->generate('Партнёры', 'partners', 'партнёров');
        $this->generate('Системы оплаты', 'payment_systems', 'систем оплаты');
        $this->generate('Записи', 'posts', 'записей');
        $this->generate('Отзывы', 'reviews', 'отзывов');
        $this->group('Настройки', function() {
            $this->permission('settings:scripts', 'Изменение скриптов');
            $this->permission('settings:social', 'Изменение социальных сетей');
            $this->permission('settings:api', 'Изменение API настроек');
        });
    }

    protected function group($name, $callback)
    {
        $this->group = $name;
        $callback();
        $this->group = null;
    }

    protected function generate($group, $prefix, $nameSuffix)
    {
        $this->group($group, function() use($prefix, $nameSuffix) {
            $this->permission($prefix . ':browse', 'Просмотр ' . $nameSuffix);
            $this->permission($prefix . ':create', 'Изменение ' . $nameSuffix);
            $this->permission($prefix . ':delete', 'Удаление ' . $nameSuffix);
            $this->permission($prefix . ':update', 'Изменение ' . $nameSuffix);
        });
    }

    protected function permission($slug, $name)
    {
        $permission = Permission::query()->where('slug', $slug)->first();
        if (!$permission) {
            $permission = new Permission;
            $permission->slug = $slug;
        }
        $permission->name = $name;
        $permission->group = $this->group;
        $permission->save();
    }
}
