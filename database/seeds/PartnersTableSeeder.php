<?php

use App\Partner;
use Illuminate\Database\Seeder;

class PartnersTableSeeder extends Seeder
{
    protected $paymentSystems = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->loan('еКапуста', function(Partner $partner) {
            $partner->image = (string) url('assets/images/logos/ekapusta.jpg');
            $partner->rate = 0.0;
            $partner->min = 100;
            $partner->max = 30000;
            $partner->min_term = 7;
            $partner->max_term = 21;
            $partner->min_age = 18;
            $partner->max_age = 70;

            $this->paymentSystems(['bank_account', 'visa', 'master_card', 'qiwi', 'yandex_money', 'contact', 'koronapay',]);
        });
        $this->loan('Турбозайм', function(Partner $partner) {
            $partner->image = (string) url('assets/images/logos/turbozaim.jpg');
            $partner->rate = 1.0;
            $partner->min = 3000;
            $partner->max = 50000;
            $partner->min_term = 7;
            $partner->max_term = 24 * 7;
            $partner->min_age = 21;
            $partner->max_age = 65;

            $this->paymentSystems(['visa', 'master_card', 'mir', 'yandex_money']);
        });
        $this->loan('Займер', function(Partner $partner) {
            $partner->image = (string) url('assets/images/logos/zaymer.jpg');
            $partner->rate = 1.0;
            $partner->min = 2000;
            $partner->max = 30000;
            $partner->min_term = 7;
            $partner->max_term = 30;
            $partner->min_age = 18;
            $partner->max_age = 75;

            $this->paymentSystems(['visa', 'master_card', 'mir', 'qiwi', 'yandex_money', 'contact', 'koronapay',]);
        });
        $this->loan('Ezaem', function(Partner $partner) {
            $partner->image = (string) url('assets/images/logos/ezaem.jpg');
            $partner->rate = 1.0;
            $partner->min = 2000;
            $partner->max = 30000;
            $partner->min_term = 5;
            $partner->max_term = 35;
            $partner->min_age = 21;
            $partner->max_age = 65;

            $this->paymentSystems(['visa', 'master_card', 'mir', 'contact',]);
        });
        $this->loan('МигКредит', function(Partner $partner) {
            $partner->image = (string) url('assets/images/logos/migkredit.png');
            $partner->rate = 0.27;
            $partner->min = 3000;
            $partner->max = 100000;
            $partner->min_term = 3;
            $partner->max_term = 365;
            $partner->min_age = 21;
            $partner->max_age = 65;

            $this->paymentSystems(['visa', 'master_card', 'mir', 'contact', 'koronapay',]);
        });
        $this->loan('Moneza', function(Partner $partner) {
            $partner->image = (string) url('assets/images/logos/moneza.jpg');
            $partner->rate = 0.0;
            $partner->min = 3000;
            $partner->max = 15000;
            $partner->min_term = 5;
            $partner->max_term = 30;
            $partner->min_age = 21;
            $partner->max_age = 65;

            $this->paymentSystems(['visa', 'master_card', 'mir', 'contact', 'qiwi',]);
        });
        $this->loan('До Зарплаты', function(Partner $partner) {
            $partner->image = (string) url('assets/images/logos/do-zarplaty.png');
            $partner->rate = 0.0;
            $partner->min = 2000;
            $partner->max = 20000;
            $partner->min_term = 7;
            $partner->max_term = 365;
            $partner->min_age = 20;
            $partner->max_age = 60;

            $this->paymentSystems(['bank_account', 'visa', 'master_card', 'mir', 'contact', 'koronapay', 'cash',]);
        });
        $this->loan('LIME', function(Partner $partner) {
            $partner->image = (string) url('assets/images/logos/lime.png');
            $partner->rate = 1.0;
            $partner->min = 2000;
            $partner->max = 70000;
            $partner->min_term = 16;
            $partner->max_term = 168;
            $partner->min_age = 21;
            $partner->max_age = 65;

            $this->paymentSystems(['bank_account', 'visa', 'master_card', 'contact', 'yandex_money', 'qiwi']);
        });
        $this->loan('Joymoney', function(Partner $partner) {
            $partner->image = (string) url('assets/images/logos/joymoney.png');
            $partner->rate = 1.5;
            $partner->min = 3000;
            $partner->max = 30000;
            $partner->min_term = 5;
            $partner->max_term = 30;
            $partner->min_age = 23;
            $partner->max_age = 60;

            $this->paymentSystems(['visa', 'master_card', 'maestro']);
        });
        $this->loan('Webbankir', function(Partner $partner) {
            $partner->image = (string) url('assets/images/logos/webbankir.png');
            $partner->rate = 0.0;
            $partner->min = 3000;
            $partner->max = 30000;
            $partner->min_term = 7;
            $partner->max_term = 31;
            $partner->min_age = 24;

            $this->paymentSystems(['bank_account', 'visa', 'master_card', 'contact', 'yandex_money']);
        });
        $this->loan('Честное Слово', function(Partner $partner) {
            $partner->image = (string) url('assets/images/logos/chestnoe-slovo.jpg');
            $partner->rate = 1.0;
            $partner->min = 3000;
            $partner->max = 10000;
            $partner->min_term = 15;
            $partner->max_term = 60;
            $partner->min_age = 18;
            $partner->max_age = 75;

            $this->paymentSystems(['bank_account', 'visa', 'master_card']);
        });
        $this->loan('Kredito24', function(Partner $partner) {
            $partner->image = (string) url('assets/images/logos/kredito24.png');
            $partner->rate = 1.0;
            $partner->min = 2000;
            $partner->max = 15000;
            $partner->min_term = 16;
            $partner->max_term = 30;
            $partner->min_age = 22;
            $partner->max_age = 60;

            $this->paymentSystems(['bank_account', 'visa', 'master_card']);
        });
        $this->loan('CreditPlus', function(Partner $partner) {
            $partner->image = (string) url('assets/images/logos/creditplus.jpg');
            $partner->rate = 0.0;
            $partner->min = 1000;
            $partner->max = 15000;
            $partner->min_term = 5;
            $partner->max_term = 30;
            $partner->min_age = 23;
            $partner->max_age = 65;

            $this->paymentSystems(['bank_account', 'visa', 'master_card', 'mir', 'contact']);
        });
        $this->loan('MoneyMan', function(Partner $partner) {
            $partner->image = (string) url('assets/images/logos/moneyman.png');
            $partner->rate = 0.0;
            $partner->min = 1000;
            $partner->max = 80000;
            $partner->min_term = 5;
            $partner->max_term = 18 * 7;
            $partner->min_age = 20;
            $partner->max_age = 65;

            $this->paymentSystems(['bank_account', 'visa', 'master_card', 'contact', 'koronapay', 'unistream']);
        });
        $this->loan('Creditstar', function(Partner $partner) {
            $partner->image = (string) url('assets/images/logos/creditstar.png');
            $partner->rate = 1.0;
            $partner->min = 1000;
            $partner->max = 30000;
            $partner->min_term = 7;
            $partner->max_term = 30;
            $partner->min_age = 18;
            $partner->max_age = 75;

            $this->paymentSystems(['bank_account', 'visa', 'master_card', 'mir']);
        });
        $this->loan('Konga', function(Partner $partner) {
            $partner->image = (string) url('assets/images/logos/konga.png');
            $partner->rate = 0.0;
            $partner->min = 2500;
            $partner->max = 20000;
            $partner->min_term = 16;
            $partner->max_term = 30;
            $partner->min_age = 18;
            $partner->max_age = 70;

            $this->paymentSystems(['bank_account', 'visa', 'master_card']);
        });
    }

    protected function partner($type, $name, $callback)
    {
        $partner = new Partner;
        $partner->type = $type;
        $partner->name = $name;
        $callback($partner);
        $partner->save();

        $pivot = [];
        foreach($this->paymentSystems as $slug) {
            $pivot[] = [
                'partner_id' => $partner->id,
                'payment_system_id' => \App\PaymentSystem::query()->where('slug', $slug)->firstOrFail()->id,
            ];
        }
        DB::table('partner_payment_systems')->insert($pivot);
        $this->paymentSystems = [];
    }

    protected function paymentSystems($systems) {
        $this->paymentSystems = $systems;
    }

    protected function loan($name, $callback)
    {
        $this->partner(Partner::TYPE_LOAN, $name, $callback);
    }
}
