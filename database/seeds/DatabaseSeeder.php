<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(MessagesTableSeeder::class);
        $this->call(FaqTableSeeder::class);
        $this->call(ReviewsTableSeeder::class);
        $this->call(PaymentSystemsTableSeeder::class);
        $this->call(PartnersTableSeeder::class);
    }
}
