<?php

use App\Lang;
use App\Slug;
use App\Page;
use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->page('Главная страница', '/', false, \App\Http\PageRenderers\HomeRenderer::class);
        $this->page('Блог', '/blog', true, \App\Http\PageRenderers\BlogRenderer::class);
        $this->page('Микрозаймы', '/loan', true, \App\Http\PageRenderers\ScriptRenderer::class, ['script' => 'assets/js/loan.js'], function(Page $page) {
            $page->content = <<<EOT
<p>Наша уникальная система поможет вам подобрать наиболее подходящую организацию для микрозайма. Введите в форме ниже необходимую сумму займа и 
срок кредитования и система вам подскажет какие организации могут вам предоставить такие условия по кредитованию.</p>
<p>Адекватно расщитывайте свои силы и возможности, что бы не попасть в долговую яму в случае не уплаты вовремя. Это может стать непосильной ношей для 
вашего буджета. Наш сервис поможет вам сравнить МФО и выбрать наилучшее предложение на рынке.</p>
EOT;
        });
        $this->page('Займ на карту', '/card', true,  \App\Http\PageRenderers\ScriptRenderer::class, ['script' => 'assets/js/card.js']);
        $this->page('Займ на эл. кошелёк', '/online', true,  \App\Http\PageRenderers\ScriptRenderer::class, ['script' => 'assets/js/online.js']);
        $this->page('Кредит наличными', '/cash', true,  \App\Http\PageRenderers\ScriptRenderer::class, ['script' => 'assets/js/cash.js']);
        $this->page('Кредитные карты', '/credit-card', true, \App\Http\PageRenderers\ScriptRenderer::class, ['script' => 'assets/js/credit-card.js']);
        $this->page('Курсы валют', '/courses', true, \App\Http\PageRenderers\CoursesRenderer::class);
        $this->page('FAQ', '/faq', true, \App\Http\PageRenderers\FaqRenderer::class, function(Page $page) {
            $page->content = <<<EOT
<p>Здесь собраны все часто задаваемые вопросы наших пользователей, мы постарались на них ответить. Если у вас есть какие то вопросы ознакомтесь с эти списком
быть может ваш вопрос здесь есть.</p>
EOT;
        });
        $this->page('Отзывы', '/reviews', true, \App\Http\PageRenderers\ReviewsRenderer::class, function(Page $page) {
            $page->content = <<<EOT
<p>Здесь собраны отзывы пользователей нашего сервиса.</p>
EOT;
        });
        $this->page('О нас', '/about', true, null, function(Page $page) {
            $page->content = <<<EOT
<p>Наш сервис помогаем подобрать кредитную огранизацию, помогает сравнить процентные ставки и сроки кредитования.</p>
<p>Невкоем случае нельзя вестись на первую попавшуюся МФО, это могут оказаться машинники. Мы проверяем все кредитные организации перед
добавление на сайт, дабы максимально обезопасить нашего пользователя.</p>
<p>Наша уникальная <a href="/loan">система</a> поможет вам выбрать организацию с подходящей суммой кредитования и сроками, а так 
же сравнить несколько и выбрать организацию с минимальной процентной ставкой.</p>
<p>Стоит реально оценивать свои силы и сроки, иначе можно легко влезть в долговую яму. Важно вовремя делать выплаты по кредиту
иначе это может гразить штрафными санкциями.</p>
EOT;
        });
    }

    /**
     * @param string $title
     * @param string $slug
     * @param bool [$hasContent]
     * @param string [$renderer]
     * @param array [$details]
     * @param callable [$callback]
     */
    protected function page($title, $slug, $hasContent = true, $renderer = null, $details = [], $callback = null)
    {
        $page = Page::where('slug', $slug)->first();
        if (!$page) {
            $page = new Page;
            $page->title = $title;
            $page->seo_title = $title;
            $page->is_system = true;
            $page->is_contentable = $hasContent;
            $page->slug = $slug;
            if ($renderer !== null) {
                $page->renderer = $renderer;
            }
            $page->details = $details;
            if (is_callable($callback)) {
                $callback($page);
            }
            $page->save();
        } else {
            if ($renderer !== null) {
                $page->renderer = $renderer;
            }
            $page->details = $details;
            $page->save();
        }
    }
}
