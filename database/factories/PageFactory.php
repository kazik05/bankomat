<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Slug;
use App\Page;
use Faker\Generator as Faker;

$factory->define(Page::class, function (Faker $faker) {
    return [
        'title' => $faker->text(50),
        'content' => $faker->text(500),
        'robots_noindex' => $faker->boolean,
        'robots_nofollow' => $faker->boolean,
        'slug' => Slug::normalize($faker->slug),
    ];
});
