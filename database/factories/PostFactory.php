<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Slug;
use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->text(50),
        'content' => $faker->text(500),
        'description' => $faker->text(100),
        'image' => url('/assets/images/theme/light/img-' . rand(1, 10) . '-800x600.jpg'),
        'robots_noindex' => $faker->boolean,
        'robots_nofollow' => $faker->boolean,
        'slug' => Slug::normalize($faker->slug),
    ];
});
