<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Review;
use Faker\Generator as Faker;

$factory->define(Review::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'review' => $faker->text(200),
        'image' => url('/assets/images/theme/light/thumb-' . rand(1, 3) . '.jpg'),
        'rating' => rand(1, 5),
    ];
});
