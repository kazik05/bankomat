let mix = require('laravel-mix');

mix.setPublicPath('public/assets')
    .js('resources/js/admin/admin.js', 'js')
    .sass('resources/scss/admin.scss', 'css')
    .js('resources/js/front/loan.js', 'js')
    .js('resources/js/front/credit-card.js', 'js')
    .js('resources/js/front/online.js', 'js')
    .js('resources/js/front/cash.js', 'js')
    .js('resources/js/front/card.js', 'js')
    .sass('resources/scss/front.scss', 'css')
    .scripts([
        // Purpose core
        'resources/libs/jquery/dist/jquery.min.js',
        'resources/libs/bootstrap/dist/js/bootstrap.bundle.min.js',
        'resources/libs/in-view/dist/in-view.min.js',
        'resources/libs/sticky-kit/dist/sticky-kit.min.js',
        'resources/libs/svg-injector/dist/svg-injector.min.js',
        'resources/libs/jquery.scrollbar/jquery.scrollbar.min.js',
        'resources/libs/jquery-scroll-lock/dist/jquery-scrollLock.min.js',
        'resources/libs/imagesloaded/imagesloaded.pkgd.min.js',
        // Plugins
        'resources/libs/swiper/dist/js/swiper.min.js',
        'resources/libs/@fancyapps/fancybox/dist/jquery.fancybox.min.js',
        'resources/libs/typed.js/lib/typed.min.js',
        // Purpose
        'resources/js/purpose/license.js',
        'resources/js/purpose/layout.js',
        'resources/js/purpose/init/*.js',
        'resources/js/purpose/custom/*.js',
        'resources/js/purpose/maps/*.js',
        'resources/js/purpose/libs/*.js',
        'resources/js/purpose/charts/*.js'
    ], 'public/assets/js/front.js')
    .copy('resources/img', 'public/assets/images')
    .copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/assets/webfonts');

// Options

mix.options({
    processCssUrls: false
});


// Live reload

// mix.browserSync({
//	 browser: 'Google Chrome',
//	 proxy: false,
//	 server: {
//		 baseDir: './',
//	 },
// 	 files: [
//         '**/*.html',
//         '**/*.js',
//         '**/*.css'
//     ]
// });


// Production settings

if (mix.inProduction()) {

}
